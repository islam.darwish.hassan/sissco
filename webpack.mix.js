const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.js('resources/js/app.js', 'public/js')
 .js('resources/muze/js/theme-custom.js', 'public/js')
 .sass('resources/scss/bootstrap.scss', 'public/css/bootstrap7.css')
 .sass('resources/muze/scss/theme.scss', 'public/css/theme2.css');

