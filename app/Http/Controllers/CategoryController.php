<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function show(Request $request ,Category $category)
    {
        $category= $category->load(['products','services']);
        $categories=$category->childs;
        return view('category',compact('category','categories'));
    }
}
