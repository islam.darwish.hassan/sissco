<?php

namespace App\Http\Controllers;

use App\Models\Services;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //
    public function show(Request $request ,Services $service){

        return view('service',compact('service'));
    }
}
