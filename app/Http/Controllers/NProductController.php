<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class NProductController extends Controller
{
    //
    public function show(Request $request ,Products $product){

        return view('product',compact('product'));
    }
}
