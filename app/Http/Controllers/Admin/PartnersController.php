<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewPartner;
use App\Http\Requests\Admin\CreateNewPartnerRequest;
use App\Http\Requests\Admin\UpdateNewRequest;
use App\Http\Requests\Admin\UpdatePartnerRequest;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnersController extends Controller
{

    public function index(Partner $partners, Request $request)

    {
        $data = Partner::dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name", "description"], $request->search_input);

        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.partners.index', compact('data'));
    }


    public function store(CreateNewPartnerRequest $request)
    {
        $record = Partner::create([
            "name" => $request->name,
            "ar_name" => $request->ar_name,
            "fr_name" => $request->fr_name,
            "description" => $request->description,
            "ar_description" => $request->ar_description,
            "fr_description" => $request->fr_description,

            "image" =>  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null,
        ]);
        $record->save();

        return redirect()->back()->with(["message" => "You have add a new partner"]);
    }
    public function update(Partner $partner, UpdatePartnerRequest $request)
    {

        $partner->name = $request->name;
        $partner->ar_name = $request->ar_name;
        $partner->fr_name = $request->fr_name;
        $partner->description = $request->input('description' . $partner->id);
        $partner->ar_description = $request->input('ar_description' . $partner->id);
        $partner->fr_description = $request->input('fr_description' . $partner->id);
        if (empty($request->new_image)) {
        } else {
            $partner->image =  $request->file('new_image') ? FileHandler::store_img($request->new_image, 'general_images') : null;
        }

        $partner->save();
        return redirect()->back()->with(["message" => "You have update the partner information"]);
    }


    public function show(Partner $partner)
    {
        $partner->name;
        $partner->ar_name;
        $partner->fr_name;
        $partner->description;
        $partner->image;
    }

    public function destroy(Partner $partner)
    {
        $partner->delete();
        return redirect()->back()->with(["message" => "You have delete a partner"]);
    }
}
