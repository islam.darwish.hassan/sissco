<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Requests\Admin\UpdateProfileRequest;

use App\Http\Requests\Admin\CreateNewProfileRequest ;
use App\Http\Requests\Admin\UpdateProfilePasswordRequest;
use App\Http\Requests\ShowUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //check dates
        $data = User::dates($request->dates);
        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.profile.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function updatePassword()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(UpdateProfilePasswordRequest $request,User $profile)
    {
        $profile->name =$profile->name;
        $profile->phone = $profile->phone;


        if(Hash::chech($request->password,$profile->password))
        {
            $profile->password =bcrypt($request->NewPassword) ;
            $profile->save();
            return redirect()->back()->with(["message" => 'the password changed']);


        }
       
        else{
            return redirect()->back()->with(["message" => 'the old password is wrong  ']);

        }
       



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request, User $profile)
    {


        $profile->name = $request->name;
        $profile->phone = $request->phone;
        

            $profile->save();
          


        return redirect()->back()->with(["message" => 'You have update your info']);


        // if($request->newpassword!=""){
        //     if(Hash::check($request->password,$user->password)){
        //         $user->password = bcrypt($request->newpassword);
        //     }else{
        //         return redirect()->back()->with(["message" => 'ادخل باسورد صحيح']);

        //     }    
        // }
        // $user->save();

        // return redirect()->back()->with(["message" => 'تم تغيير الباسورد']);

    }
        
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

  
}
