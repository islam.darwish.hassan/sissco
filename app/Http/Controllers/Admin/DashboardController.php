<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Award;
use App\Models\Category;
use App\Models\Customer;
use App\Models\News;
use App\Models\Partner;
use App\Models\Products;
use App\Models\Request as ModelsRequest;
use App\Models\Services;
use App\Models\Vacancy;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $months = array();

        $values = array();
        $values2 = array();
        $values3 = array();
        $values4 = array();
        $values5 = array();
        $values6 = array();
        $values7 = array();
        $values8 = array();
        $values9 = array();
        $values10 = array();
        $values11 = array();
        $values12 = array();

        for ($i = 6; $i >= 0; $i--) {

            array_push($months, Carbon::now()->subMonths($i)->format('M'));
            array_push(
                $values,
                Category::whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values2,
                Products::whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values3,
                Award::whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values4,
                Services::whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values5,
                News::whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values6,
                Vacancy::whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values7,
                ModelsRequest::whereYear('created_at', '=', Carbon::now())->whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values12,
                ModelsRequest::whereYear('created_at', '=', Carbon::now()->subYear(1))->whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );

            array_push(
                $values8,
                Partner::whereYear('created_at', '=', Carbon::now())->whereMonth('created_at', '=', Carbon::now()->subWeeks(52)->subMonths($i))->get()->count()
            );
            array_push(
                $values11,
                Partner::whereYear('created_at', '=', Carbon::now()->subYear(1))->whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );

            array_push(
                $values9,
                Customer::whereYear('created_at', '=', Carbon::now())->whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );
            array_push(
                $values10,
                Customer::whereYear('created_at', '=', Carbon::now()->subYear(1))->whereMonth('created_at', '=', Carbon::now()->subMonths($i))->get()->count()
            );



        }
        $chart_1_number = Category::count();
        $chart_1 = [["name" => " مكتمل ", "data" => $values, 'categories' => $months]];
        $chart_2_number = Products::count();
        $chart_2 = [["name" => " مكتمل ", "data" => $values2, 'categories' => $months]];
        $chart_3_number = Award::count();
        $chart_3 = [["name" => " مكتمل ", "data" => $values3, 'categories' => $months]];
        $chart_4_number = Services::count();
        $chart_4 = [["name" => " مكتمل ", "data" => $values4, 'categories' => $months]];
        $chart_5_number = News::count();
        $chart_5 = [["name" => " مكتمل ", "data" => $values5, 'categories' => $months]];
        $chart_6_number = Vacancy::count();
        $chart_6 = [["name" => " مكتمل ", "data" => $values6, 'categories' => $months]];
        $chart_7_number = ModelsRequest::count();
        $chart_7 = [["name" => " Requests ", "data" => $values7, 'categories' => $months]];
        $chart_8_number = Partner::count();
        $chart_8 = [["name" => " Partners ", "data" => $values8, 'categories' => $months]];
        $chart_9 = [["name" => " Requests ", "data" => $values9, 'categories' => $months]];
        $chart_10 = [["name" => " Requests ", "data" => $values10, 'categories' => $months]];
        $chart_11 = [["name" => " Partners ", "data" => $values11, 'categories' => $months]];
        $chart_12 = [["name" => " Requests ", "data" => $values12, 'categories' => $months]];

        return view('admin.dashboard', compact(
            'chart_1',
            'chart_1_number',
            'chart_2',
            'chart_2_number',
            'chart_3',
            'chart_3_number',
            'chart_4',
            'chart_4_number',
            'chart_5',
            'chart_5_number',
            'chart_6',
            'chart_6_number',
            'chart_7',
            'chart_7_number',
            'chart_8',
            'chart_8_number',
            'chart_9',
            'chart_10',
            'chart_11',
            'chart_12',

            'months'
        ));
    }
}
