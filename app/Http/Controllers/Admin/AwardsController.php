<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewAwardsRequest;
use App\Http\Requests\Admin\UpdateAwardsRequest;
use App\Models\Award;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

use function PHPUnit\Framework\isNull;

class AwardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //check dates
        $data = Award::dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name"], $request->search_input);
        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.awards.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewAwardsRequest $request)
    {

        $record = Award::create([
            "name" => $request->name,
            "ar_name" => $request->ar_name,
            "fr_name" => $request->fr_name,
            "description" => $request->description,
            "ar_description" => $request->ar_description,
            "fr_description" => $request->fr_description,
            "image" =>  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null,
        ]);
        $record->save();

        return redirect()->back()->with(["message" => "You have add a new award"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Award $award)
    {
        $award->name;
        $award->ar_name;
        $award->fr_name;
        $award->description;
        $award->image;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Award $award)
    {
        $award->name = $request->name;
        $award->ar_name = $request->ar_name;
        $award->fr_name = $request->fr_name;
        $award->description = $request->input('description' . $award->id);
        $award->ar_description = $request->input('ar_description' . $award->id);
        $award->fr_description = $request->input('fr_description' . $award->id);
        if (empty($request->new_image)) {
        } else {
            $award->image =  $request->file('new_image') ? FileHandler::store_img($request->new_image, 'general_images') : null;
        }

        $award->save();
        return redirect()->back()->with(["message" => "You have update the award information"]);
    }

    /*
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, Award $award)
    {
        //
        $award->delete();
        return redirect()->back()->with(["message" => 'Award has been deleted']);
    }
    public function ezz(Request $request)
    {
    }
}
