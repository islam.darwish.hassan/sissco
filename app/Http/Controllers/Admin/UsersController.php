<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Requests\ShowUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //check dates
        $data = User::dates($request->dates);
        $data=$data->searchIn(["name","email","phone"],$request->search_input);

        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.users.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewUserRequest $request, User $user)
    {


        $user = new User();
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->save();

        return redirect()->back()->with(["message" => 'تم حذف الشهاده بنجاح']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        $user->name;
        $user->email;
        $user->phone;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->name = $request->name;
        $user->phone = $request->phone;
        if($request->newpassword != "")
        {
            $user->password = bcrypt($request->newpassword);
            return redirect()->back()->with(["message" => 'تم تغيير الباسورد']);

        }


            $user->save();

        return redirect()->back()->with(["message" => 'تم تغيير د']);


        // if($request->newpassword!=""){
        //     if(Hash::check($request->password,$user->password)){
        //         $user->password = bcrypt($request->newpassword);
        //     }else{
        //         return redirect()->back()->with(["message" => 'ادخل باسورد صحيح']);

        //     }    
        // }
        // $user->save();

        // return redirect()->back()->with(["message" => 'تم تغيير الباسورد']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();
        return redirect()->back()->with(["message" => 'تم حذف الشهاده بنجاح']);
    }

    public function profile(User $user)
    {
        
    }
}
