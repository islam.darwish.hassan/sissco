<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewVacancyRequest;

use App\Models\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VacanciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //check dates
        $data = Vacancy::dates($request->dates);
        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.vacancies.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *                                                                                                          
     * @param  \Illuminate\Http\Request  $request                               
     * @return \Illuminate\Http\Response                                            
     */
    public function store(CreateNewVacancyRequest $request)
    {
        //
        // $this->authorize('view-super', Auth::user());
        $record = Vacancy::create([
            "title" => $request->title,
            "ar_title" => $request->ar_title,
            "fr_title" => $request->fr_title,

            "description" => $request->description,
            "ar_description" => $request->ar_description,
            "fr_description" => $request->fr_description,

            "image" =>  FileHandler::store_img($request->image, 'general_images'),
            "small_image" =>  FileHandler::store_img($request->image, 'general_images'),
        ]);
        $record->save();

        return redirect()->back()->with(["message" => "تم اضافة فرصه عمل جديده"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Vacancy $vacancy)
    {
        $vacancy->title = $request->title;
        $vacancy->ar_title = $request->ar_title;
        $vacancy->fr_title = $request->fr_title;
        $vacancy->description = $request->input('description' . $vacancy->id);
        $vacancy->ar_description = $request->input('ar_description' . $vacancy->id);
        $vacancy->fr_description = $request->input('fr_description' . $vacancy->id);
        if (empty($request->image)) {
        } else {
            $vacancy->image =  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null;
        }
        if (empty($request->small_image)) {
        } else {
            $vacancy->small_image =  $request->file('small_image') ? FileHandler::store_img($request->small_image, 'general_images') : null;
        }


        $vacancy->save();
        return redirect()->back()->with(["message" => "You have update the vacancy information"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Vacancy $vacancy)
    {
        $vacancy->delete();
        return redirect()->back()->with(["message" => 'Vacancy has been deleted']);
    }
}
