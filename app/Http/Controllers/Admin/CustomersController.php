<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Models\Customer;
use App\Http\Requests\Admin\CreateNewCustomerRequest;
use App\Http\Requests\Admin\UpdateCustomerRequest;

use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function index(Customer $customers, Request $request)
    {
        $data = Customer::dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name", "description"], $request->search_input);

        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();
        return view('admin.customers.index', compact('data'));
    }


    public function store(CreateNewCustomerRequest $request)
    {
        $record = Customer::create([
            "name" => $request->name,
            "ar_name" => $request->ar_name,
            "fr_name" => $request->fr_name,
            "description" => $request->description,
            "ar_description" => $request->ar_description,
            "fr_description" => $request->fr_description,
            "image" =>  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null,
        ]);
        $record->save();

        return redirect()->back()->with(["message" => "You have add a new Customer"]);
    }


    public function show(Customer $customer)
    {
        $customer->name;
        $customer->ar_name;
        $customer->fr_name;
        $customer->description;
        $customer->image;
    }



    public function update(Customer $customer, UpdateCustomerRequest $request)
    {

        $customer->name = $request->name;
        $customer->ar_name = $request->ar_name;
        $customer->fr_name = $request->fr_name;
        $customer->description = $request->input('description' . $customer->id);
        $customer->ar_description = $request->input('ar_description' . $customer->id);
        $customer->fr_description = $request->input('fr_description' . $customer->id);
        if (empty($request->new_image)) {
        } else {
            $customer->image =  $request->file('new_image') ? FileHandler::store_img($request->new_image, 'general_images') : null;
        }

        $customer->save();
        return redirect()->back()->with(["message" => "You have update the customer information"]);
    }




    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect()->back()->with(["message" => "You have delete a customer"]);
    }
}
