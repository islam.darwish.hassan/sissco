<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;

use App\Http\Requests\Admin\CreateNewNewRequest;
use App\Http\Requests\Admin\UpdateNewRequest;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index(Request $request)
    {

        //check dates
        $data = News::dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name"], $request->search_input);

        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.news.index', compact('data'));
    }
    public function store(CreateNewNewRequest $request, News $news)
    {
        $news->name = $request->name;
        $news->ar_name = $request->ar_name;
        $news->fr_name = $request->fr_name;
        $news->description = $request->description;
        $news->ar_description = $request->ar_description;
        $news->fr_description = $request->fr_description;

        $news->image =  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null;

        $news->save();
        return redirect()->back()->with(["message" => "You have add a new post"]);
    }
    public function show(News $news)
    {
        $news->name;
        $news->ar_name;
        $news->fr_name;
        $news->description;
        $news->image;
    }
    public function update(UpdateNewRequest $request, News $news)
    {
        $news->name = $request->name;
        $news->ar_name = $request->ar_name;
        $news->fr_name = $request->fr_name;
        $news->description = $request->input('description' . $news->id);
        $news->ar_description = $request->input('ar_description' . $news->id);
        $news->fr_description = $request->input('fr_description' . $news->id);

        $news->image =  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null;

        $news->save();
        return redirect()->back()->with(["message" => "You have update a post"]);
    }
    public function destroy(Request $request, News $news)
    {
        //
        $news->delete();
        return redirect()->back()->with(["message" => 'Post has been deleted']);
    }
}
