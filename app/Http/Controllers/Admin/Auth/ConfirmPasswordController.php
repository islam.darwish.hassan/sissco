<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Errors;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Foundation\Auth\ConfirmsPasswords;

class ConfirmPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Confirm Password Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password confirmations and
    | uses a simple trait to include the behavior. You're free to explore
    | this trait and override any functions that require customization.
    |
    */


    public function login(Request $request)
    {
         $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
         $user = User::where('email', strtolower($request->email))->where('role', User::ADMIN)->first();
        if (!$user)
            return redirect()->route('login')->with('error', Errors::NOT_FOUND_USER);

        if($user)
        if (!Hash::check($request->password, $user->password))
        return redirect()->route('login')->with('error', Errors::WRONG_PASSWORD);

        if($user)
        Auth::login($user);

        $role = Auth::user()->role;

        // Check user role
        switch ($role) {
            case '1':
                return redirect()->route('dashboard.index');
            break;
            default:
            return redirect()->route('admin_login_form')->with('error', Errors::WRONG_PASSWORD);
        break;
        }

    }
}
