<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewServiceRequest;
use App\Http\Requests\Admin\UpdateServices;
use App\Http\Requests\Admin\UpdateServicesRequest;
use App\Models\Category;
use App\Models\Services;
use Illuminate\Http\Request;

class ServicesController extends Controller
{

    public function index(Services $services, Category $categories, Request $request)
    {
        $categories = Category::all();
        $data = Services::dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name", "description"], $request->search_input);

        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.services.index', compact('data', 'categories'));
    }

    public function store(CreateNewServiceRequest $request, Services $service)
    {
        $record = Services::create([
            "name" => $request->name,
            "ar_name" => $request->ar_name,
            "fr_name" => $request->fr_name,
            "description" => $request->description,
            "ar_description" => $request->ar_description,
            "fr_description" => $request->fr_description,
            "category_id" => $request->category_id,
            "image" =>  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null,
        ]);
        $record->save();

        return redirect()->back()->with(["message" => "You have add a new service"]);
    }





    public function update(UpdateServicesRequest $request, Services $service)
    {

        $service->name = $request->name;
        $service->ar_name = $request->ar_name;
        $service->fr_name = $request->fr_name;
        $service->description = $request->input('description'.$service->id);
        $service->ar_description = $request->input('ar_description'.$service->id);
        $service->fr_description = $request->input('fr_description'.$service->id);

        $service->category_id = $request->category_id;

        if (empty($request->new_image)) {
        } else {
            $service->image =  $request->file('new_image') ? FileHandler::store_img($request->new_image, 'general_images') : null;
        }

        $service->save();
        return redirect()->back()->with(["message" => "You have update the service information"]);
    }




    public function destroy(Services $service)
    {
        $service->delete();
        return redirect()->back()->with(["message" => 'Service has been deleted']);
    }

    public function preview(Services $service,Request $request,Category $categories)
    {
        $categories=Category::all();
        $service = Services::where('id',$request->id)->get();

        return view('admin.services.preview',compact('service','categories'));
    }
}
