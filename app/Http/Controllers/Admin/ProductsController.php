<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewProductRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request, Products $products, Category $categories)
    {

        //check dates
        $categories = Category::all();
        $data = Products::dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name", "description"], $request->search_input);

        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.products.index', compact('data', 'categories'));
    }


    public function store(CreateNewProductRequest $request, Products $products, Category $category)
    {

        $record = Products::create([
            "name" => $request->name,
            "ar_name" => $request->ar_name,
            "fr_name" => $request->fr_name,
            "description" => $request->description,
            "ar_description" => $request->ar_description,
            "fr_description" => $request->fr_description,
            "category_id" => $request->category_id,
            "image" =>  $request->file('image') ? FileHandler::store_img($request->image, 'general_images') : null,
        ]);
        $record->save();

        return redirect()->back()->with(["message" => "You have add a new product"]);
    }



    public function show(Products $products)
    {

        $products->name;
        $products->ar_name;
        $products->fr_name;
        $products->description;
        $products->category_id;
    }



    public function update(UpdateProductsRequest $request, Products $product)
    {

        $product->name = $request->name;
        $product->ar_name = $request->ar_name;
        $product->fr_name = $request->fr_name;
        $product->description = $request->input('description'.$product->id);
        $product->ar_description = $request->input('ar_description'.$product->id);
        $product->fr_description = $request->input('fr_description'.$product->id);
        $product->category_id = $request->category_id;

        if (empty($request->new_image)) {
        } else {
            $product->image =  $request->file('new_image') ? FileHandler::store_img($request->new_image, 'general_images') : null;
        }

        $product->save();
        return redirect()->back()->with(["message" => "You have update the product information"]);
    }


    public function destroy(Products $product)
    {
        $product->delete();
        return redirect()->back()->with(["message" => 'Product has been deleted']);
    }


    public function preview(Request $request, Products $product, Category $categories)
    {

        //check dates
        $categories = Category::all();
        $data = Products::where('id', $request->id)->get();;

        return view('admin.products.preview', compact('data', 'categories'));
    }
}
