<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateNewCategory;
use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isEmpty;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = Category::withCount('childs');

        //check dates
        $data = $data->dates($request->dates);
        $data = $data->searchIn(["name", "ar_name", "fr_name"], $request->search_input);
        $data = $data->sortable('id', 'desc')
            ->paginate(15)->withQueryString();;
        return view('admin.categories.index', compact('data'));
    }
    public function Products()
    {
        return $this->hasMany(Products::class);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewCategory $request)
    {
        $category = new Category();
        $category->name = $request->en_name;
        $category->ar_name = $request->ar_name;
        $category->fr_name = $request->fr_name;
        $category->description = $request->description;
        $category->ar_description = $request->ar_description;
        $category->fr_description = $request->fr_description;
        $category->image = $request->file('image') ? FileHandler::store_img($request->image, 'categories_images') : null;
        $category->cover = $request->file('cover') ? FileHandler::store_img($request->cover, 'categories_images') : null;
        $category->save();
        return redirect()->back()->with(["message" => "Category added successfully!"]);
    }

    public function storesub(CreateNewCategory $request, Category $parent)
    {
        $category = new Category();
        $category->name = $request->en_name;
        $category->ar_name = $request->ar_name;
        $category->fr_name = $request->fr_name;
        $category->parent_id = $parent->id;
        $category->image = $request->file('image') ? FileHandler::store_img($request->image, 'categories_images') : null;
        $category->cover = $request->file('cover') ? FileHandler::store_img($request->cover, 'categories_images') : null;
        $category->save();
        $category->description =  $request->input('description' . $parent->id);
        $category->ar_description = $request->input('ar_description' . $parent->id);
        $category->fr_description = $request->input('fr_description' . $parent->id);
        $category->save();

        return redirect()->back()->with(["message" => "Category added successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Category $category)
    {
        //
        $data = $category->childs()->paginate();
        return view('admin.categories.show', compact('category', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->name = $request->name;
        $category->ar_name = $request->ar_name;
        $category->description = $request->input('description' . $category->id);
        $category->ar_description = $request->input('ar_description' . $category->id);
        $category->fr_description = $request->input('fr_description' . $category->id);
        if ($request->hasFile('image')) {
            Storage::disk('categories_images')->delete($category->image);
            $category->image = FileHandler::store_img($request->image, 'categories_images');
        } else {
        }
        $category->save();
        return  redirect()->back()->with('message', 'تم تعديل القسم بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        if ($category->childs->isEmpty()) {
            $category->delete();
            return redirect()->back()->with(['message' => 'Category deleted Successfully!']);
        } else {

            foreach ($category->childs as $item) {
                $item->delete();
            }
            $category->delete();
            return redirect()->back()->with(['message' => 'Category and its subcategories deleted Successfully!']);
        }
    }
}
