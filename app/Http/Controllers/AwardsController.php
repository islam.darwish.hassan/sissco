<?php

namespace App\Http\Controllers;

use App\Models\Award;
use Illuminate\Http\Request;

class AwardsController extends Controller
{
    //
    public function index(){
        $awards=Award::all();
        return view('awards',compact('awards'));
    }
}
