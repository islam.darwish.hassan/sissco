<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //
    public function index(Request $request ,News $new){

        $news = News::paginate(10);
        return view('news',compact('news'));
    }
    public function show(Request $request ,News $new){

        return view('new',compact('new'));
    }
}
