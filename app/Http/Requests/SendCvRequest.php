<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendCvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>"required|min:1|max:200",
            'major'=>"required|min:1|max:200",
            'address'=>"required|min:1|max:200",
            'file'=>"required|between:1,5000",

        ];
    }
}
