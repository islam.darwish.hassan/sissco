<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>"required|string|min:1|max:255",
            'ar_name'=>"string|min:1|max:255",
            'fr_name'=>"string|min:1|max:255",
            'description'=>"string|min:1",
            'new_image'=>"image|mimes:jpeg,png,jpg,gif,svg|max:2048"        ];
    }
}
