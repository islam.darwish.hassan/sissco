<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewPartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //

            'name'     => "required|min:1|max:100",
            'ar_name'     => "min:1|max:100",
            'fr_name'     => "min:1|max:100",

            'image'      => "required|image",
            'description'    => 'nullable|min:5|max:250',

        ];

    }

}
