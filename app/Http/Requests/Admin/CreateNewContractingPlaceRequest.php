<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewContractingPlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'     => 'required|min:1|max:100',
            'ar_name'     => 'required|min:1|max:100',
            'image'      => 'nullable|image',
            'description'    => 'nullable|min:5|max:250',
            'contact_email'=> 'nullable|max:190|email',
            'contact_phone'=>"nullable|string|min:5|max:12",
            'description' => "nullable|string|min:1|max:250",
        ];
    }
    
}
