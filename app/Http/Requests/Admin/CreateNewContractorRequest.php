<?php

use App\Models\Category;
use App\Models\Contractor;

namespace App\Models;

namespace App\Http\Requests\Admin;
use Illuminate\Foundation\Http\FormRequest;

class CreateNewContractorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categories = Category::all()->implode('id', ',');

        return [
            //
            'name' => "required|string|min:1|max:100",
            'phone'=>"required|string|min:5|max:12",
            'district'=>"required|string|min:5|max:20",
            'category_id'         => 'in:' . $categories,
            'description' => "nullable|string|min:1|max:250",
            "workers_count" => "nullable|string|min:1|max:250",
            

        ];
    }
    
}
