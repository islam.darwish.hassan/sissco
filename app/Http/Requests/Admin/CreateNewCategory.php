<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'ar_name' => "nullable|string|min:1|max:100",
            'en_name' => "required|string|min:1|max:100",
            'fr_name' => "nullable|string|min:1|max:100"

        ];
    }

}
