<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewVacancyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'     => "required|min:1|max:100",
            'ar_title'     => "required|min:1|max:100",
            'description'     => "required|min:1|max:65530",
            'image'      => "required|image",
            'small_image'      => "required|image",

        ];
    }

}
