<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            //
            'ar_name' => "required|string|min:1|max:100",
            'name' => "nullable|string|min:1|max:100",
            'phone'=>"nullable|string|min:5|max:12",
            'email'=> 'nullable|unique:users|max:190|email',
            'address' => "nullable|string|min:1|max:250",
            'description' => "nullable|string|min:1|max:250",

            

        ];
    }
    public function messages()
    {
        return [
            'ar_name.required' => ' مطلوب اسم المنشأه العربي',
        ];
    }
}
