<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "name"=>"required|min:2|max:250",
            "email"=>'required|unique:users|min:5|max:250',
            "password"=>'required|min:6|max:200',
            "phone"=>'nullable|between:7,15',
            "address"=>'nullable|string|max:500',
            "role"=>"required|between:1,4"
        ];
    }
}
