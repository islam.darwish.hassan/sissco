<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendContactMail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required|string|max:200',
            'title'=>'required|string|max:200',
            'email'=>'required|email|max:250',
            'title'=>'required|string|max:200',
            'phone'=>'required|between:7,15',
            'message'=>'required|string|max:400',

        ];
    }
}
