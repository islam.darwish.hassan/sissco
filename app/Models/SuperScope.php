<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SuperScope extends Model
{
    public function scopeDates($query, $value)
    {

        if ($value == 'today')  return $query->whereDay('created_at', now());
        if ($value == "yesterday") return $query->where('created_at', now()->subDay());

        if ($value == "week") return $query->whereBetween('created_at', [now()->startOfWeek()->format('Y-m-d H:i'), now()->endOfWeek()->format('Y-m-d H:i')]);
        if ($value == "month") return $query->whereMonth('created_at', now()->month);
        if ($value == "past") return $query->where('created_at', '<', now());
    }
    public function scopeSearchIn($query,$array,$value)
    {
        if($value!=""){
            foreach($array as $element)
            {
                   $query=$query->orWhere($element,"Like","%".$value."%"); 
            }
            return $query;
        }

    }
}
