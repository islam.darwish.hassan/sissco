<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Banner extends SuperScope
{
    use HasFactory;
    use Sortable;


    protected $fillable = ['title', 'ar_title', 'image','order','url'];

    public $sortable = [
        'id',
        'title', 'ar_title','order'
    ];
// public function getImageAttribute($value){
//     return asset('files/general/images/'.$value);
// }
}
