<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use Sortable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeDates($query, $value)
    {

        if ($value == 'today')  return $query->whereDay('created_at', now());
        if ($value == "yesterday") return $query->where('created_at', now()->subDay());

        if ($value == "week") return $query->whereBetween('created_at', [now()->startOfWeek()->format('Y-m-d H:i'), now()->endOfWeek()->format('Y-m-d H:i')]);
        if ($value == "month") return $query->whereMonth('created_at', now()->month);
        if ($value == "past") return $query->where('created_at', '<', now());
    }
    public function scopeSearchIn($query,$array,$value)
    {
        if($value!=""){
            foreach($array as $element)
            {
                   $query=$query->orWhere($element,"Like","%".$value."%"); 
            }
            return $query;
        }

    }

}
