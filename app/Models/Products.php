<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Products extends SuperScope
{
    use HasFactory;
    use Sortable;


    protected $fillable = ['name', 'ar_name', 'fr_name','description','image','fr_description','ar_description','category_id'];

    public $sortable = [
        'id',
        'name', 'ar_name','fr_name','description',
    ];
    public function Category()
{
    return $this->belongsTo(Category::class);
}
public function getImageAttribute($value){
    return asset('files/general/images/'.$value);
}
}
