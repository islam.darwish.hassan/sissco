<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Customer extends SuperScope
{
    use HasFactory;
    use HasFactory;
    use Sortable;

    protected $fillable = ['name', 'ar_name', 'description', 'fr_description', 'ar_description', 'image', 'fr_name'];

    public $sortable = [
        'id',
        'name', 'ar_name',
    ];
    public function getImageAttribute($value)
    {
        return asset('files/general/images/' . $value);
    }
}
