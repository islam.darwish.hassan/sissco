<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Award extends SuperScope
{
    use HasFactory;
    use Sortable;


    protected $fillable = ['name', 'ar_name', 'description', 'ar_description', 'fr_description', 'image', 'fr_name'];

    public $sortable = [
        'id',
        'name', 'ar_name'
    ];
    public function getImageAttribute($value)
    {
        return asset('files/general/images/' . $value);
    }
}
