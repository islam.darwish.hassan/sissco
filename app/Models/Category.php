<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends SuperScope
{
    use HasFactory;
    use Sortable;

    protected $fillable = ['name', 'ar_name','fr_name', 'parent_id','description','ar_description','fr_description'];

    public $sortable = [
        'id',
        'name', 'ar_name','fr_name'
    ];
    public $sortableAs = ['childs_count'];

    public function parent()
    {
        return $this->belongsTo(\App\Models\Category::class, 'parent_id');
    }
    public function childs()
    {
        return $this->hasMany(\App\Models\Category::class, 'parent_id');
    }
    public function Products()
    {
        return $this->hasMany(Products::class);
    }
    public function services()
    {
        return $this->hasMany(Services::class);
    }
    public static function parents(){
        self::whereNull('parent_id');

    }
    public function getImageAttribute($value){
        if(!$value) return asset('covers/cover1.png');
        return asset('files/categories/images/'.$value);
    }
    public function getCoverAttribute($value){
        if(!$value) return asset('covers/cover2.png');
        return asset('files/categories/images/'.$value);
    }

}
