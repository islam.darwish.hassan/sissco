<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Request extends SuperScope
{
    use HasFactory;
    use Sortable;
    public $sortable = [
        'id',
        'name', 'ar_name'
    ];

}
