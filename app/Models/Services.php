<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Services extends SuperScope
{
    use HasFactory;
    use Sortable;


    protected $fillable = ['name', 'ar_name', 'fr_name', 'description', 'image', 'category_id', 'ar_description', 'fr_description'];

    public $sortable = [
        'id',
        'name', 'ar_name', 'fr_name', 'description',
    ];
    public function Category()
    {
        return $this->belongsTo(Category::class);
    }
    public function getImageAttribute($value)
    {
        return asset('files/general/images/' . $value);
    }
}
