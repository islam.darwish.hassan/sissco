<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Vacancy extends SuperScope
{
    use HasFactory;
    use Sortable;


    protected $fillable = ['title', 'ar_title',  'fr_title','description', 'ar_description', 'fr_description', 'image', 'small_image'];

    public $sortable = [
        'id',
        'title', 'ar_title'
    ];
    public function getImageAttribute($value)
    {
        return asset('files/general/images/' . $value);
    }

    public function getSmallImageAttribute($value)
    {
        return asset('files/general/images/' . $value);
    }
}
