<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('ar_name')->nullable();
            $table->string('fr_name')->nullable();
            $table->string('description')->nullable();            
            $table->string('ar_description')->nullable();
            $table->string('fr_description')->nullable();
            $table->string('image')->nullable();
            $table->string('small_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutomsers');
    }
}
