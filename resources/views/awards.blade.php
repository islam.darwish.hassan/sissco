@extends('layouts.website')
@section('content')

@include('_includes.navbar')
@include('_includes.topnav')

<img src={{asset('covers/cover4.jpeg')}} class="image-cover"/>
<x-w_subheader>
    <h2 class="p-2 fw-bold"> Awards</h2>
</x-w_subheader>

<div class="awards">
  <section class="cards-wrapper">
    @foreach ($awards as $row )
        

    <div class="card-grid-space ltr">
      <a class="card" href={{$row->image}} style="--bg-img: url({{$row->image}})">
        <div class="">
          <h1>{{$row->name}}</h1>
          <p>{{$row->description}}</p>
        </div>
      </a>
    </div>
    @endforeach

  </section>
</div>
@endsection