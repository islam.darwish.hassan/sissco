@extends('layouts.admin')
@section('main-content')

<div class="signup-template bg-gray-100">

    <div class="position-absolute top-0 start-0 p-4 p-md-5">
        <a href="#" class="ps-lg-3"><img src="{{ asset('logo.png') }}" alt="logo"></a>
    </div>
    <div class="row g-0 align-items-center">
        <div class="col-lg-7">
            <img src="{{ asset('assets/img/signin-cover.jpg') }}" class="cover-fit" alt="Sign in Cover">
        </div>
        <div class="col-lg-5 px-md-3 px-xl-5">
            <div class="px-3 py-4 p-md-5 p-xxl-5 mx-xxl-4">
                <div class="login-form py-2 py-md-0 mx-auto mx-lg-0">
                    <h2 class="h1 mb-3">تسجيل الدخول</h2>

                    <form method="POST" action="{{ route('login_submit') }}" class="pt-2">
                        @csrf
                        <div class="mb-4 pb-md-2">
                            <label class="form-label form-label-lg" for="Username">اسم المستخدم</label>
                            <input id="email" type="email" name="email"
                                class="form-control form-control-xl @error('email') is-invalid @enderror"
                                value="{{ old('email') }}" required autocomplete="email" placeholder="البريد الالكترونى"
                                autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                        <div class="mb-4 pb-md-2">
                            <label class="form-label form-label-lg" for="Password">كلمة السر</label>
                            <input id="password" type="password"
                                class="form-control form-control-xl @error('password') is-invalid @enderror" name="password"
                                required autocomplete="current-password" placeholder="كلمة المرور">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="d-grid">
                            <button type="submit" class="btn btn-xl btn-primary">تسجيل الدخول</button>
                        </div>
                        <div class="my-4 d-flex pb-3">
                            <a href="#" class="small text-gray-600 ms-auto mt-1">نسيت كلمه المرور</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
