<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Siescom</title>
    <link rel="icon" href="{{asset('icon.png')}}" type="image/png">
    <link rel="stylesheet" href="{{asset('css/bootstrap7.css')}}">
</head>
<body>

    @include('_includes.navbar')
    @include('_includes.navigation')
    @yield('content')
    @include('_includes.footer')
</body>
<script src="{{asset('js/app.js')}}"></script>
</html>

