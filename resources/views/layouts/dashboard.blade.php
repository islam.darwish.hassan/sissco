@extends('layouts.admin')
<div class=" bg-gray-100 ">
    @section('main-content')
    @include('admin._includes.sidebar')

    <div class="main-content ">
        @include('_includes.errors')
        @yield('content')
    </div>
    @endsection
</div>
