<!DOCTYPE html>
<html >

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Siescom Dashboard</title>
    <link href="{{ asset('assets/vendor/quill/dist/quill.snow.css') }}" rel="stylesheet" type="text/css" media="all">

    <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/prism/prism.css')}}">
    <link rel="stylesheet" href="{{ asset('css/theme2.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="{{asset('assets/vendor/simplebar/dist/simplebar.min.css')}}" rel="stylesheet" type="text/css" media="all">

</head>

<body class="bg-gray-100 "  >
    @yield('main-content')
</body>
<script src="{{ asset('js/theme-custom.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/lodash.min.js') }}"></script>
<script src="{{ asset('js/highmaps.js') }}"></script>
<script src="{{ asset('js/apexcharts.min.js') }}"></script>
<script src="{{ asset('js/simplebar.min.js') }}"></script>
@stack('extra_scripts')
</html>
