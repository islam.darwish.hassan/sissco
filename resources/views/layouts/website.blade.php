
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Siescom</title>
    <!-- Fonts -->
<link rel="icon" href="{{asset('icon.png')}}" type="image/png">
<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{asset('css/bootstrap7.css')}}">
<link rel="stylesheet" href="{{asset('css/theme2.css')}}">
<link rel="stylesheet" href="{{asset('css/animate.css')}}">
<style>
    iframe{
bootstrap7    }
</style>

</head>

<body class="antialiased ">
    @yield('content')
</body>
<footer>
    <x-footer />

</footer>
<script src="js/app.js"></script>
<script src="{{asset('js/wow.min.js')}}"></script>
<script>
new WOW({mobile:       false       }).init();
</script>


</html>
