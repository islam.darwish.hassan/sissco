@extends('layouts.website')
@section('content')

@include('_includes.navbar')
@include('_includes.topnav')

<img src={{asset('covers/cover5.jpeg')}} class="image-cover"/>

<x-w_subheader>
    <h2 class="p-2 fw-bold"> News</h2>
</x-w_subheader>
<div class="container">
<div class="row">
@foreach ($news as $row )
<div class="col-lg-6 col-sm-12  py-5">
    <div class="newsCard ">
        <p class="title">{{$row->name}} </p>
        <small>{{\Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->diffForHumans()}}</small>
        <div class="content">{!!$row->description!!} </div>

            <div class="more d-flex ">
                    <a href={{route('newsa.show',$row->id)}}  class="px-2" >More</a>
                    <x-svg-morearrow/>
            </div>

    </div>

</div>
@endforeach
</div>
</div>
@endsection