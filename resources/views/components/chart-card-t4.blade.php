@props(['id' => 'chart', 'title' => '', 'datasets' => null, 'number' => '', 'labels' => null])
<div class="col-md-6 col-lg-12 col-xl-6 col-xxl-2">
    <div class="card mb-4 rounded-12 shadow">
        <div class="card-body p-3 p-xl-3 p-xxl-4">
            <div class="row align-items-center">
                <div class="col-5 col-xxl-9">
                    <span class="caption text-gray-600 d-block mb-1">{{ $title }}</span>
                    <span class="h3 mb-0">{{ $number }}</span>

                </div>
            </div>
        </div>
    </div>
</div>
@push('extra_scripts')

    <script>
        var options = {
            series: [{
                name: @json($datasets)[0].name,
                data: @json($datasets)[0].data
            }],
            chart: {
                type: 'line',
                height: 90,
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false,
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 3,
                colors: ['#66949E', '#8FBFC9'],
                curve: 'smooth'
            },
            legend: {
                show: false,
            },
            grid: {
                show: false,
                xaxis: {
                    lines: {
                        show: false
                    }
                },
                row: {
                    colors: undefined,
                    opacity: 0
                },
            },
            tooltip: {
                enabled: true,
                marker: {
                    fillColors: ['#66949E', '#8FBFC9'],
                },
                x: {
                    show: false
                },
            },
            markers: {
                colors: ['#66949E', '#8FBFC9'],
            },
            yaxis: {
                show: false,
            },
            xaxis: {
                type: "category",
                categories: @json($labels),
                labels: {
                    show: false,
                },
                axisTicks: {
                    show: true,
                },
                axisBorder: {
                    show: false,
                },
                stroke: {
                    width: 0,
                },
                tooltip: {
                    enabled: true,
                }
            }
        };

        var target = document.querySelector("#{{ $id }}");
        var chart = new ApexCharts(target, options);
        chart.render();
    </script>
@endpush
