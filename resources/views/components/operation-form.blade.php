@props(["method"=>"" ,"action"=>""])
<form method="{{$method=='get'?'get':'post'}}" action="{{ $action }}">
    @csrf
    @if($method=='delete')@method('delete')@endif
    @if($method=='put')@method('put')@endif
    <button type="submit" class="dropdown-item">
        {{ $slot }}
    </button>

</form>
