@props(["title"=>"",'right'=>false,'image'=>""])
<div {{ $attributes->merge([ 'class' => '']) }} >
<img  style="width:100%; height:auto; object-fit:cover; border-radius:50px; max-width:25rem; max-height:25rem;" src="{{$image}}"/>
<p class="fs-5  mx-auto py-2 fw-bold text-primary text-center" style="width:100%; max-width:25rem;">{{$title}}</p>

</div>
