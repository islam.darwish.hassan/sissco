@props(['name' => '', 'id' => '', 'title' => '', 'value' => ''])

<div class=" form-group  ">
    <label class="form-label form-label-lg">{{ $title }}</label>
    <div id="{{ $id }}" class="quill-editor "></div>
</div>
<input type="hidden" name="{{ $name }}" />
<br />
