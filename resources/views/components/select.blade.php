<select {{$attributes->merge([ "class"=>"form-select form-select-lg mb-3"])}} aria-label=".form-select-lg example">
    {{$slot}}
  </select>
