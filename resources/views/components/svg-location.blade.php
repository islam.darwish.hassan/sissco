<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="37.703" height="62.84" viewBox="0 0 37.703 62.84">
    <defs>
      <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
        <stop offset="0" stop-color="#cfae5a"/>
        <stop offset="1" stop-color="#a77e40"/>
      </linearGradient>
    </defs>
    <path id="Icon_map-postal-code" data-name="Icon map-postal-code" d="M26.052,0A18.768,18.768,0,0,0,7.2,18.683C7.2,29,26.052,62.84,26.052,62.84S44.9,29,44.9,18.683A18.768,18.768,0,0,0,26.052,0ZM25.99,24.271A5.767,5.767,0,1,1,31.81,18.5,5.792,5.792,0,0,1,25.99,24.271Z" transform="translate(-7.2)" fill="url(#linear-gradient)"/>
  </svg>
  