@props(["href"=>"" ,"title" =>"" ])
<li {{ $attributes->merge([ 'class' => 'dropdown-item' ]) }}
    href="{{$href}}">
<a class="dropdown-item" href="#">
    <span>{{$title}}</span>
</a>
</li>
