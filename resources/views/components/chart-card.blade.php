@props(['id' => 'chart', 'title' => '', 'datasets' => null, 'secdatasets' => [null], 'number' => '', 'labels' => null])
<div class="col-md-6 col-lg-12 col-xl-6 col-xxl-6">
    <div class="card mb-4 rounded-12 shadow">
        <div class="card-body p-3 p-xl-3 p-xxl-4">
            <div class="row align-items-center">
                <div class="col-5 col-xxl-6">
                    <span class="caption text-gray-600 d-block mb-1">{{ $title }}</span>
                    <span class="h3 mb-0">{{ $number }}</span>

                </div>
                <div class="col-7 col-xxl-6 pe-xxl-0">
                    <div id="{{ $id }}"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('extra_scripts')

    <script>
        var options = {
            series: [{
                name: @json($datasets)[0].name + '2021',
                data: @json($datasets)[0].data
            }, {
                name: @json($secdatasets)[0].name + '2020',
                data: @json($secdatasets)[0].data
            }],
            chart: {
                type: 'line',
                height: 90,
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false,
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 3,
                colors: ['#000000', '#8B8B8B'],
                curve: 'smooth'
            },
            legend: {
                show: false,
            },
            grid: {
                show: false,
                xaxis: {
                    lines: {
                        show: false
                    }
                },
                row: {
                    colors: undefined,
                    opacity: 0
                },
            },
            tooltip: {
                enabled: true,
                marker: {
                    fillColors: ['#000000', '#8B8B8B'],

                },
                x: {
                    show: false
                },
            },
            markers: {
                colors: ['#000000', '#8B8B8B'],
            },
            yaxis: {
                show: false,
            },
            xaxis: {
                type: "category",
                categories: @json($labels),
                labels: {
                    show: false,
                },
                axisTicks: {
                    show: true,
                },
                axisBorder: {
                    show: false,
                },
                stroke: {
                    width: 0,
                },
                tooltip: {
                    enabled: true,
                }
            }
        };

        var target = document.querySelector("#{{ $id }}");
        var chart = new ApexCharts(target, options);
        chart.render();
    </script>
@endpush
