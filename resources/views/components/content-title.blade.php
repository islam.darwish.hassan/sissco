<div class="px-3 px-xxl-5 py-3 py-lg-4 border-bottom border-gray-200 after-header">
    <div class="container-fluid px-0 pt-3">
        <div class="row align-items-center">
            <div class="col">
                <h1 class="h2 mb-0">{{$title}}</h1>
            </div>
            {{$slot}}
        </div>
    </div>
</div>
