@props(['route' => ''])

<div class="dropdown export-dropdown">
    <a href="#" role="button" id="dates" data-bs-toggle="dropdown" aria-expanded="false"
        class="btn btn-outline-dark border-gray-700 text-gray-700 px-3">
        @if (request()->dates == 'today')
            <span>Today </span>
        @elseif(request()->dates=="yesterday")
            <span>Yesterday </span>
        @elseif(request()->dates=="week")
            <span>Last 7 days </span>
        @elseif(request()->dates=="month")
            <span>This Month </span>
        @else
            <span>All </span>
        @endif
        <x-svg-arrow />
    </a>
    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dates">
        <li><a href="{{ $route }} " class="dropdown-item" href="#"><span>All</span></a></li>
        <li><a href="{{ $route . '?dates=today' }} " class="dropdown-item" href="#"><span>Today</span></a></li>
        <li><a href="{{ $route . '?dates=yesterday' }} " class="dropdown-item" href="#"><span>Yesterday</span></a></li>
        <li><a href="{{ $route . '?dates=week' }} " class="dropdown-item" href="#"><span>Last 7 days</span></a></li>
        <li><a href="{{ $route . '?dates=month' }} " class="dropdown-item" href="#"><span>This Month</span></a></li>

    </ul>
</div>
