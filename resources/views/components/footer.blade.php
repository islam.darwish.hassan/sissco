<style>
    @media only screen and (max-width: 1026px) {
        #center {
            align-items: center;
        }
    }

</style>

<div class="footer text-center text-lg-start ">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row pt-5">

            <!--Grid column-->
            <div class="col-lg-3 col-md-12 mb-4 mb-md-0">
                <a href="{{route('about.us')}}">ABOUT US</a><br>
                <a href="#">BUSINESS LINES</a><br>
                <a href="{{route('awardsa.index')}}">AWARDS</a><br>
                <a href="#">GLOBAL PRESENSE</a><br>
            </div>
            <!--Grid column-->
            <div class="col-lg-3 col-md-12 mb-4 mb-md-0">
                <a href="{{route('privacy')}}">Data Privacy</a><br>
                <a href="{{route('privacy')}}">Cookie Policy</a><br>
            </div>
            <!--Grid column-->
            <div class="col-lg-6 col-md-12 mb-4 ml-3 mb-md-0">
                <div class="footer_register" id="center">
                    <div>
                        <p style="color: white;">Register for our newsletter</p>
                        <div class="footer_box" action="" method="post" style="display: flex; flex-direction:row;">
                            <input type="email" placeholder="E-mail address" aria-label="Search">
                            <x-send_icon />
                        </div>
                        <div>
                            <x-socialMedia_bar />
                        </div>
                    </div>
                </div>

            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->

    <!-- Copyright -->
</div>
<div class="text-center p-1" style="font-weight:800;">
    Copyright 2022 Siescom All rights reserved
</div>
