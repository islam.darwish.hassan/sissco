@props(["title"=>"","subtitle"=>""  ])

        <ul {{ $attributes->merge([ 'class' => 'dropdown-menu dropdown-menu-end show' ]) }}  aria-labelledby="dropdownMenuButton" style="margin: 0px; position: absolute; inset: 0px auto auto 0px; transform: translate3d(-204px, 48.5px, 0px);" wfd-invisible="true" data-popper-placement="bottom-end">
        <li class="pt-2 px-4">
        <span class="fs-16 font-weight-bold text-black-600 Montserrat-font me-2">{{$title}}</span><img src="../assets/svg/icons/fill-check.svg" alt="icon">
        <small class="text-gray-600 pb-3 d-block">{{$subtitle}}</small>
        </li>

    
    <x-avatar-list-item title="coco">
    </x-avatar-list-item>
    
    </ul>