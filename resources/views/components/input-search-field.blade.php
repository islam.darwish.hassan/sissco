@props(["title"=>"" ,"placeholder" =>"" ])
<div  {{ $attributes->merge([ 'class' => 'ps-3 header-search' ]) }} >

    <div class="input-group bg-white border border-gray-300 rounded py-1 px-3 align-items-center">
        <svg id="Icon" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
            <rect id="Icons_Tabler_Search_background" data-name="Icons/Tabler/Search background" width="14" height="14" fill="none"/>
            <path id="Combined_Shape" data-name="Combined Shape" d="M13.141,13.895l-.06-.052L9.1,9.859A5.569,5.569,0,1,1,9.859,9.1l3.983,3.983a.539.539,0,0,1-.7.813ZM1.077,5.564A4.487,4.487,0,1,0,5.564,1.077,4.492,4.492,0,0,0,1.077,5.564Z" fill="#6c757d"/>
        </svg>

        <input type="search" class="form-control border-0" placeholder={{$placeholder}}>
    </div>
    <span class="muze-search d-lg-none ms-3">
    <svg id="icons_tabler_close" data-name="icons/tabler/close" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 16 16">
        <rect data-name="Icons/Tabler/Close background" width="16" height="16" fill="none"/>
        <path d="M.82.1l.058.05L6,5.272,11.122.151A.514.514,0,0,1,11.9.82l-.05.058L6.728,6l5.122,5.122a.514.514,0,0,1-.67.777l-.058-.05L6,6.728.878,11.849A.514.514,0,0,1,.1,11.18l.05-.058L5.272,6,.151.878A.514.514,0,0,1,.75.057Z" transform="translate(2 2)" fill="#1e1e1e"/>
    </svg>
    </span>
</div>
