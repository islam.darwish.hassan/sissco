@props(["title"=>"" ,"icon"=>""])
<div  {{ $attributes->merge([ 'class' => 'd-grid' ]) }} >
    <button type="button" class="btn btn-xl btn-primary">{{$slot}}</button>
</div>
