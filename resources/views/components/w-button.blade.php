@props(["title"=>"" ,"icon"=>""])
<button  {{ $attributes->merge([ 'class' => 'btn ' ]) }} >
    {{$slot}}
  </button>
