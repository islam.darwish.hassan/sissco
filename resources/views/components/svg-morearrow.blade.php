<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 25.242">
    <g id="Icon_feather-arrow-right" data-name="Icon feather-arrow-right" transform="translate(-6 -5.379)">
      <path id="Path_165" data-name="Path 165" d="M7.5,18h21" fill="none" stroke="#695521" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
      <path id="Path_166" data-name="Path 166" d="M18,7.5,28.5,18,18,28.5" fill="none" stroke="#695521" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
    </g>
  </svg>
