@props(['title'=>''])
<div class="mb-5  px-5">
    <div class="card rounded-12 shadow-dark-80 ">
        <div class="card-body px-0">
            <div class="d-flex align-items-center border-bottom border-gray-200 pb-3 mb-2 px-3 px-md-4">
                <h5 class="card-header-title mb-0 font-weight-semibold ps-md-2">{{$title}}</h5>
                <div class="ms-auto pe-md-2">
                    {{{$header}}}

                </div>
            </div>
            <div class="card-body-inner ">
                <table class="table table-hover text-center ">
                    <thead class="bg-gray-100">
                        {{ $tableheader }}
                    </thead>
                    <tbody>
                        {{ $tablebody }}
                    </tbody>

                </table>
            </div>

        </div>
    </div>
    @if(isset($tablefooter))
    {{$tablefooter}}
@endif


</div>
