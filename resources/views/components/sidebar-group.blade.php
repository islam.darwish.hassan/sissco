@props(['title' => '', 'id' => 'collapseds'])
<li class="nav-item">
    <a class="nav-link collapsed" href="#{{ $id }}" data-bs-toggle="collapse" role="button"
        aria-expanded="false" aria-controls="{{ $id }}">
        @if (isset($icon)){{ $icon }}@endif&nbsp;<span
                class="ms-2 position-relative">{{ $title }} </span>
    </a>
    <div class="collapse collapse-box" id="{{ $id }}" data-bs-parent="#{{ $id }}">
        <ul class="nav nav-sm flex-column">
            {{ $slot }}
        </ul>
    </div>
</li>
