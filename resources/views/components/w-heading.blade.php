@props(['header'=>'','sub_header'=>''])

<div class="heading">
    <h4>{{$header}}</h4>
    <p class="pt-2">{{$slot}}</p>
</div>
