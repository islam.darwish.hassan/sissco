<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 33.621 33.621">
    <g id="Icon_feather-send" data-name="Icon feather-send" transform="translate(-1.5 -0.879)">
      <path id="Path_168" data-name="Path 168" d="M33,3,16.5,19.5" fill="none" stroke="#695521" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
      <path id="Path_169" data-name="Path 169" d="M33,3,22.5,33l-6-13.5L3,13.5Z" fill="none" stroke="#695521" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
    </g>
  </svg>
  