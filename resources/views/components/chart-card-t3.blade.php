@props(['id' => 'chart', 'title' => '', 'datasets' => null, 'labels' => null, 'secdatasets' => null])
<div class="card rounded-12 shadow-dark-80">
    <div class="card-body px-0">
        <div class="d-flex align-items-center border-bottom border-gray-200 pb-3 mb-2 px-3 px-md-4">
            <h5 class="card-header-title mb-0 font-weight-semibold ps-md-2">{{ $title }}</h5>
            <div class="ms-auto pe-md-2">
                {{-- <div class="dropdown export-dropdown">
                    <a href="#" role="button" id="UserOverview" data-bs-toggle="dropdown" aria-expanded="false"
                        class="btn btn-outline-dark border-gray-700 text-gray-700 px-3"><span>Today </span> <svg
                            class="ms-2" xmlns="http://www.w3.org/2000/svg" width="13" height="13"
                            viewBox="0 0 13 13">
                            <rect data-name="Icons/Tabler/Chevron Down background" width="13" height="13" fill="none" />
                            <path
                                d="M.214.212a.738.738,0,0,1,.952-.07l.082.07L7.1,5.989a.716.716,0,0,1,.071.94L7.1,7.011l-5.85,5.778a.738.738,0,0,1-1.034,0,.716.716,0,0,1-.071-.94l.071-.081L5.547,6.5.214,1.233A.716.716,0,0,1,.143.293Z"
                                transform="translate(13 3.25) rotate(90)" fill="#495057" />
                        </svg>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="UserOverview">
                        <li><a class="dropdown-item" href="#"><span>Today</span></a></li>
                        <li><a class="dropdown-item" href="#"><span>Yesterday</span></a></li>
                        <li><a class="dropdown-item" href="#"><span>Last 7 days</span></a></li>
                        <li><a class="dropdown-item" href="#"><span>This month</span></a></li>
                        <li><a class="dropdown-item" href="#"><span>Last month</span></a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#"><svg data-name="icons/tabler/calendar"
                                    xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                    <rect data-name="Icons/Tabler/Calendar background" width="16" height="16"
                                        fill="none" />
                                    <path
                                        d="M2.256,16A2.259,2.259,0,0,1,0,13.743V3.9A2.259,2.259,0,0,1,2.256,1.641H3.282V.616A.615.615,0,0,1,4.507.532l.005.084V1.641H9.846V.616A.615.615,0,0,1,11.071.532l.006.084V1.641H12.1A2.259,2.259,0,0,1,14.359,3.9v9.846A2.259,2.259,0,0,1,12.1,16ZM1.231,13.743a1.027,1.027,0,0,0,1.025,1.026H12.1a1.027,1.027,0,0,0,1.026-1.026V7.795H1.231Zm11.9-7.179V3.9A1.027,1.027,0,0,0,12.1,2.872H11.077V3.9a.616.616,0,0,1-1.226.084L9.846,3.9V2.872H4.513V3.9a.615.615,0,0,1-1.225.084L3.282,3.9V2.872H2.256A1.026,1.026,0,0,0,1.231,3.9V6.564Z"
                                        transform="translate(1)" fill="#495057" />
                                </svg><span class="ms-2">Custom</span></a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
        <div class="card-body-inner px-3 px-md-4">
            <div id="{{ $id }}" class="px-md-2"></div>
        </div>
    </div>
</div>
@push('extra_scripts')

    <script>
        var options = {
            series: [{
                name: '2020',
                data: @json($secdatasets)[0].data
            }, {
                name: '2021',
                data: @json($datasets)[0].data
            }],
            chart: {
                type: 'line',
                height: 380,
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false,
                }
            },
            dataLabels: {
                enabled: false,
            },
            stroke: {
                width: 3,
                colors: ['#000000', '#8B8B8B'],
                curve: 'smooth'
            },
            legend: {
                show: true,
                position: 'top',
                horizontalAlign: 'left',
                fontSize: '13px',
                fontFamily: 'Open Sans,sans-serif',
                fontWeight: 400,
                labels: {
                    colors: '#6C757D',
                },
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: ['#000000', '#8B8B8B'],

                    radius: 12,
                },
            },
            grid: {
                show: true,
                borderColor: '#E9ECEF',
                xaxis: {
                    lines: {
                        show: false
                    }
                },
                row: {
                    colors: undefined,
                    opacity: 0
                },
            },
            tooltip: {
                enabled: true,
                marker: {
                    fillColors: ['#000000', '#8B8B8B'],
                },
                x: {
                    show: false
                },
            },
            markers: {
                colors: ['#000000', '#8B8B8B'],
            },
            yaxis: {
                show: true,
                labels: {
                    style: {
                        colors: '#6C757D',
                        fontSize: '13px',
                        fontFamily: 'Open Sans,sans-serif',
                        fontWeight: 400,
                    }
                },
            },
            xaxis: {
                categories: @json($labels),
                labels: {
                    show: true,
                    style: {
                        colors: '#6C757D',
                        fontSize: '13px',
                        fontFamily: 'Open Sans,sans-serif',
                        fontWeight: 400,
                    }
                },
                axisTicks: {
                    show: false,
                },
                axisBorder: {
                    show: false,
                },
                stroke: {
                    width: 0,
                },
                tooltip: {
                    enabled: false,
                }
            }
        };


        var target = document.querySelector("#{{ $id }}");
        console.log(target);
        var chart = new ApexCharts(target, options);
        chart.render();
    </script>
@endpush
