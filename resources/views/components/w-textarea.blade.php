@props(["label"=>''])
<div class="mb-2 pb-md-2 ">
    @if(isset($label))
    <div class="w-input-label">
        <label class="form-label form-label-lg" >{{$label}}</label>
    </div>
    @endif
    <textarea style="white-space: normal" {{$attributes->merge(["class"=>"form-control shadow-sm"])}}>
        {{$slot}}
    </textarea>

</div>

