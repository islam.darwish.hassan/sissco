@props(["image"=>null ,"title"=>""])

<div class="job-card">
    <img src="{{$image}}"/>
    <p class="py-2">{{$title}}</p>
</div>
