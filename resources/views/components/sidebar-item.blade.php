@props(["title"=>"" , "route"=>""])
<li class="nav-item">
    <a {{$attributes->merge(["class"=>"nav-link"])}}  href="{{$route}}" >
        {{$slot}}
        &nbsp;<span class="ms-2">{{$title}}</span>
    </a>
  </li>
