@props(["label"=>null])
<div class="w-input ">
    @if(isset($label))
    <div class="w-input-label">
        <h4 >{{$label}}</h4>
    </div>
    @endif
    <input {{$attributes->merge(["class"=>"form-control shadow-sm"])}}/>

</div>

