
<div class=" businessContainer" >
    <div class="imageGrid row ">
        @foreach ( $categories as $category )
            
        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class=" wow fadeInDown imageCard mt-2" data-wow-delay="{{strval($loop->index*100)."ms"}}">
                <img src="{{ $category->image }}"  alt=" ...">
                <div class="{{$loop->index<6?"wow slideInLeft":""}} captionContainer" onclick="window.location='{{ route('category.show',$category->id ) }}'">
                    <p >{{$category->name}}</p>
                    <img src="{{ asset('images/Icon feather-arrow-right@2x.png') }}" style="height: 20px">
                </div>


                <div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
