@props(['id' => null, 'value' => null, 'arabic' => null, 'frensh' => null, 'rowid' => null])
{{-- {{ $slot }}
@if ($slot)) --}}
<x-rich-input id="editor{{ $id }}" name="description{{ $rowid }}" title="Description*" />
<x-rich-input id="editor_fr{{ $id }}" name="fr_description{{ $rowid }}" title="FR Description" />
<x-rich-input id="editor_ar{{ $id }}" name="ar_description{{ $rowid }}" title="AR Description" />
{{-- @endif --}}

@push('extra_scripts')
    <link href="https://cdn.quilljs.com/2.0.0-dev.4/quill.snow.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/styles/github.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.3.1/highlight.min.js"></script>

    <script src="{{ asset('assets/vendor/quill/dist/quill.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/quill/image-resize.min.js') }}"></script>
    <script src="https://unpkg.com/quill-html-edit-button@2.2.7/dist/quill.htmlEditButton.min.js"></script>

    <script>
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike', 'link'], // toggled buttons
            [{
                'header': 1
            }, {
                'header': 2
            }], // custom button value
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }], // superscript/subscript
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }], // outdent/indent
            [{
                'direction': 'rtl'
            }], // text direction

            [{
                'color': []
            }, {
                'background': []
            }], // dropdown with defaults from theme
            [{
                'align': []
            }], , ['image'],
            ['blockquote'],
            ['video'],
            [{
                'size': ['small', false, 'large', 'huge']
            }], // custom dropdown

            ['clean'] // remove formatting button

        ];
        Quill.register("modules/htmlEditButton", htmlEditButton);

        var editor_ar{{ $id }} = new Quill('#editor_ar{{ $id }}', {
            theme: 'snow',
            placeholder: 'Enter Description Here',
            modules: {
                imageResize: {
                    displaySize: true
                },
                toolbar: toolbarOptions,
                htmlEditButton: {
                    debug: true,
                    syntax: true
                },

            },
        });
        var editor{{ $id }} = new Quill('#editor{{ $id }}', {
            theme: 'snow',
            placeholder: 'Enter Description Here',
            modules: {
                htmlEditButton: {
                    syntax: true
                },
                imageResize: {
                    displaySize: true
                },

                toolbar: toolbarOptions,
            }
        });
        var editor_fr{{ $id }} = new Quill('#editor_fr{{ $id }}', {
            theme: 'snow',
            placeholder: 'Enter Description Here',
            modules: {
                htmlEditButton: {
                    syntax: true
                },
                imageResize: {
                    displaySize: true
                },

                toolbar: toolbarOptions,
            },
        });
        @if ($value)
            const value{{ $id }} = '{!! html_entity_decode($value) !!}';
            const delta{{ $id }} = editor{{ $id }}.clipboard.convert(value{{ $id }})
            editor{{ $id }}.setContents(delta{{ $id }}, 'slient');
        @endif
        @if ($arabic)
            const arabic{{ $id }} = '{!! html_entity_decode($arabic) !!}';
            const ar_delta{{ $id }} = editor_ar{{ $id }}.clipboard.convert(arabic{{ $id }})
            editor_ar{{ $id }}.setContents(ar_delta{{ $id }}, 'slient');
        @endif
        @if ($frensh)
            const frensh{{ $id }} = '{!! html_entity_decode($frensh) !!}';
            const fr_delta{{ $id }} = editor_fr{{ $id }}.clipboard.convert(frensh{{ $id }})
            editor_fr{{ $id }}.setContents(fr_delta{{ $id }}, 'slient');
        @endif

        @if ($id)var form = document.querySelector("#{{ $id }}");
        @else var form = document.querySelector('.richform');
        @endif

        form.onsubmit = function(e) {
            // // Populate hidden form on submit
            var ar_description = document.querySelector('input[name=ar_description{{ $rowid }}]');
            var description = document.querySelector('input[name=description{{ $rowid }}]');
            var fr_description = document.querySelector('input[name=fr_description{{ $rowid }}]');
            fr_description.value = editor_fr{{ $id }}.root.innerHTML;
            ar_description.value = editor_ar{{ $id }}.root.innerHTML;
            description.value = editor{{ $id }}.root.innerHTML;
        };
    </script>


@endpush
