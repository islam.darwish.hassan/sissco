@props(["title"=>"" ,"icon" =>"" ])
<div {{ $attributes->merge([ 'class' => 'dropdown export-dropdown' ]) }}>
    <a href="#" role="button" id="UserOverview" data-bs-toggle="dropdown" aria-expanded="true" class="btn btn-outline-dark border-gray-700 text-gray-700 px-3 show">
        <span>{{$title}} </span> 
    <x-svg-arrow/>
    </a>
        <ul class="dropdown-menu dropdown-menu-end show" aria-labelledby="UserOverview" style="margin: 0px; position: absolute; inset: 0px auto auto 0px; transform: translate3d(-111px, 42.5px, 0px);" data-popper-placement="bottom-end">
            {{$slot}}
        </ul>
    </div>


