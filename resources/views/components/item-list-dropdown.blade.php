@props(["title"=>"","href"=>"", "data-mame" => "","icon" =>"" ])
<li  {{ $attributes->merge([ 'class' => 'dropdown-item' ]) }} >
    <span>{{$slot}}</span>
 </li>
 