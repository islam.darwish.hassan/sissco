<svg class="ms-2"
            xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
            <rect data-name="Icons/Tabler/Chevron Down background" width="13" height="13" fill="none" />
            <path
                d="M.214.212a.738.738,0,0,1,.952-.07l.082.07L7.1,5.989a.716.716,0,0,1,.071.94L7.1,7.011l-5.85,5.778a.738.738,0,0,1-1.034,0,.716.716,0,0,1-.071-.94l.071-.081L5.547,6.5.214,1.233A.716.716,0,0,1,.143.293Z"
                transform="translate(13 3.25) rotate(90)" fill="#495057" />
        </svg>
