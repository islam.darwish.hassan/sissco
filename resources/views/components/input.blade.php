@props(['label' => '', 'value' => ''])
<div class=" mb-2 pb-md-2">
    <label class="form-label form-label-lg" for="FirstName">{{ $label }} </label>
    @php
        // dd($value)
    @endphp
    <input {{ $attributes->merge(['class' => 'form-control form-control-xlg']) }} value="{{$value}}">
</div>
