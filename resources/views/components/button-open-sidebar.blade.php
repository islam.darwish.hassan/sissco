@props(["title"=>"","href"=>"" ,"icon" =>"" ])
<a  {{ $attributes->merge([ 'class' => 'back-arrow bg-white circle circle-sm
shadow border border-gray-200 rounded mb-0' ]) }}
    href="javascript:void(0);"   >
    <x-icon-arrow-open-sidebar>
    </x-icon-arrow-open-sidebar>
</a>
