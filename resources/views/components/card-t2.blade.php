@props(["title"=>"",'right'=>false,'footer'=>""])
<div class=" bg-light card-t2">
    <div class =" py-5 container  @if($right)flex-row-reverse  @endif">
        <div class="text-center " >
            <h5 class="pb-lg-3 text-primary fw-bold fs-3">{{$title}}</h5>
            {{$slot}}
        </div>
        <h5 class="footer ">{{$footer}}</h5>
        </div>

</div>
