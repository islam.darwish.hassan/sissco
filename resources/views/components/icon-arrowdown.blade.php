<div class="dropdown">

    <svg class="  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false"
        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8.254" height="4.719"
        viewBox="0 0 8.254 4.719">
        <defs>
            <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                <stop offset="0" stop-color="#cfae5a" />
                <stop offset="1" stop-color="#a77e40" />
            </linearGradient>
        </defs>
        <path id="Icon_ionic-ios-arrow-down" data-name="Icon ionic-ios-arrow-down"
            d="M10.316,14.543l3.121-3.123a.587.587,0,0,1,.833,0,.6.6,0,0,1,0,.835l-3.536,3.539a.589.589,0,0,1-.813.017L6.36,12.258a.59.59,0,0,1,.833-.835Z"
            transform="translate(-6.188 -11.246)" fill="url(#linear-gradient)" />
    </svg>
    <ul class="dropdown-menu dropdown-menu-end " aria-labelledby="dropdownMenuButton1">
        <li><a class="dropdown-item" href="#">English</a></li>
        <li><a class="dropdown-item" href="#">Frensh</a></li>
        <li><a class="dropdown-item" href="#">Arabic</a></li>
    </ul>
</div>
