@props(["label"=>''])
{{--  <div class=" mb-2 pb-md-2">
    <label class="form-label form-label-lg" for="FirstName">{{$label}}</label>
    <input {{$attributes->merge(["class"=>"form-control form-control-xl"])}}   >
  </div>  --}}
  <div class="row">
    <div class="col">
      <label class="form-label form-label-lg" for="FirstName">{{$label}}</label>
      <input {{ $attributes->merge(["class"=>"form-control"]) }} >
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Last name" aria-label="Last name">
    </div>
  </div>
  