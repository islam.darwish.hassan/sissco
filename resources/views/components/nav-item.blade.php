@props(["name"=>'' ,"link"=>"home" ,"active"=>false ])
<li {{ $attributes->merge([ 'class' => 'nav-item']) }} >
    <a class="nav-link  @if(request()->routeIs($link)) active @endif" href="{{route($link)}}">{{$name}}</a>
  </li>
