@props(["title"=>"",'right'=>false,'footer'=>""])
    <div class =" contianer py-5    @if($right)flex-row-reverse  @endif">
        <div class="text-center " >
            <h3 class="pb-lg-3  text-primary fw-bold fs-3 mb-5 "><u>{{$title}}</u></h3>
            <div class="container px-5">
                <div class="row ">
                    {{$slot}}
                </div>

            </div>
        </div>
        <h5 class="footer ">{{$footer}}</h5>
    </div>

</div>
