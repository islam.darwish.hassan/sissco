@props(["data-id"=>"", "data-url"=>"", "id"=>""])
<button {{ $attributes->merge(['class' => ' dropdown-item']) }}>
    {{ $slot }}
</button>
