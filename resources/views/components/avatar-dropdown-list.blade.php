@props(["title"=>"","avatar"=>"" ,"icon" =>"" ])

<div {{ $attributes->merge([ 'class' => 'dropdown profile-dropdown' ]) }} >


    {{-- avatar section  --}}
    <a href="#" class="avatar avatar-sm avatar-circle ms-4 ms-xxl-5 show" data-bs-toggle="dropdown" aria-expanded="true" id="dropdownMenuButton">
    <img class="avatar-img" src={{$avatar}} alt="Avatar">
    </a>
    {{-- avatar section --}}


    <x-avatar-list title="Mahmoud Fox" subtitle="fox@hotmail.com">
    </x-avatar-list>

    </div>    