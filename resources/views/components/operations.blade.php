<a href="#" class="btn btn-dark-100 btn-icon btn-sm rounded-circle" role="button" data-bs-toggle="dropdown"
    aria-haspopup="true" aria-expanded="false">
    <x-svg-3dots />
</a>
<div class="dropdown-menu dropdown-menu-end">
{{$slot}}
</div>
