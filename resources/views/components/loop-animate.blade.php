@if($loop)
<div {{ $attributes->merge([ 'class' => 'wow fadeInDown' ])->except('loop') }} data-wow-delay="{{strval($loop->index*100)."ms"}}">
{{$slot}}
</div>
@endif