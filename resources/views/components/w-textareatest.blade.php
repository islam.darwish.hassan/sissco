@props(["label"=>''])
<div class=" mb-2 pb-md-2">
    <label class="form-label form-label-lg" for="FirstName">{{$label}}</label>
    <textarea {{$attributes->merge(["class"=>"form-control form-control-xlg"])}}   >
  </div>
