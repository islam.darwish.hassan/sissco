@props(["title"=>"","image"=>"test",'right'=>false,'button'=>'Read More..' ,'rtl'=>false])

<div class ="card-t1 row my-5  @if($right)flex-row-reverse bg-light @endif">
<div class="col-7 col-md-6 p-lg-5 px-3 pt-5 my-auto" >
    <h3 class="pb-lg-3 text-primary text-bold ">{{$title}}</h3>
    {{$slot}}
{{$button_??""}}
</div>
<div class=" col-5 col-md-6 px-0 my-auto " >

    @if(!$rtl)
    <img src="{{$image}}" class="w-100  @if($right)  round-end
    @else round-start @endif "
    style="min-height:300px; max-height:740px;object-fit: cover;
    " />
    @else
    <img src="{{$image}}" class="w-100  @if($right) round-start
    @else round-end @endif "
    style="min-height:300px; max-height:740px;object-fit: cover;

    " />

    @endif
</div>
</div>
