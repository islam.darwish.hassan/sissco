@extends('layouts.dashboard')
@section('content')
    <x-arrow-header />
    @foreach ($service as $row)

        <div class="mb-5  px-5">
            <div class="card rounded-12 shadow-dark-80 ">
                <div class="card-body px-0">
                    <div class="d-flex align-items-center border-bottom border-gray-200 pb-3 mb-2 px-3 px-md-4">
                        <h5 class="card-header-title mb-0 font-weight-semibold ps-md-2">Service preview</h5>
                        <div class="ms-auto pe-md-2">
                        </div>

                    </div>
                    <div class="m-3">
                        <img src="{{ $row->image }}" class=" border rounded-circle" style="object-fit: cover;"
                            width="200px" height="200px" alt="...">
                    </div>
                    <div class="card-body-inner align-items-center">
                        <table class="table table-hover text-center ">
                            <nav class="justify-content-center">
                                <div class="nav nav-tabs justify-content-around" id="nav-tab" role="tablist">

                                    <a class="nav-link " id="nav-ardescrition-tab" data-bs-toggle="tab"
                                        href="#nav-ar_description" role="tab" aria-controls="nav-home" aria-selected="true">
                                        <h6>Arabic</h6>
                                    </a>
                                    <a class="nav-link " id="nav-endescrition-tab" data-bs-toggle="tab"
                                        href="#nav-en_description" role="tab" aria-controls="nav-home" aria-selected="true">
                                        <h6>English</h6>
                                    </a>
                                    <a class="nav-link " id="nav-home-tab" data-bs-toggle="tab"
                                        href="#nav-fr_description" role="tab" aria-controls="nav-home" aria-selected="true">
                                        <h6>Frensh</h6>
                                    </a>

                                </div>

                            </nav>
                            <div class="tab-content mt-3" id="nav-tabContent">
                                <div class="tab-pane fade show " id="nav-ar_description" role="tabpanel"
                                    aria-labelledby="nav-home-tab">

                                    <div class="m-3">
                                        <div class="row">
                                            <div class="col-sm">
                                                <x-input label="Ar name" type="text" value="{{ $row->ar_name }}"
                                                    disabled />
                                            </div>
                                            <div class="col-sm">
                                                <x-input label="Category" type="text" value="{{ $row->category->name }}"
                                                    disabled />
                                            </div>

                                        </div>

                                        <x-w-textarea label="
                                                Description" type="text" value="{{ $row->description }}" disabled>
                                                {{ strip_tags($row->ar_description) }}                                        </x-w-textarea>


                                    </div>
                                </div>
                                <div class="tab-pane fade show " id="nav-en_description" role="tabpanel"
                                    aria-labelledby="nav-home-tab">
                                    <div class="m-3">
                                        <div class="row">
                                            <div class="col-sm">
                                                <x-input label="En name" type="text" value="{{ $row->name }}" disabled />
                                            </div>
                                            <div class="col-sm">
                                                <x-input label="Category" type="text" value="{{ $row->category->name }}"
                                                    disabled />
                                            </div>

                                        </div>

                                        <x-w-textarea label="
                                                Description" type="text" value="{{ $row->description }}" disabled>
                                            {{ strip_tags($row->description) }} </x-w-textarea>


                                    </div>
                                </div>
                                <div class="tab-pane fade show " id="nav-fr_description" role="tabpanel"
                                    aria-labelledby="nav-home-tab">
                                    <div class="m-3">
                                        <div class="row">
                                            <div class="col-sm">
                                                <x-input label="Fr name" type="text" value="{{ $row->fr_name }}"
                                                    disabled />
                                            </div>
                                            <div class="col-sm">
                                                <x-input label="Category" type="text" value="{{ $row->category->name }}"
                                                    disabled />
                                            </div>

                                        </div>

                                        <x-w-textarea label="
                                                Description" type="text" value="{{ $row->description }}" disabled>
                                                {{ strip_tags($row->fr_description) }}                                        </x-w-textarea>


                                    </div>
                                </div>
                            </div>

                        </table>
                    </div>

                </div>
            </div>



        </div>

    @endforeach
@endsection
{{-- <img src="{{ $row->image }}" class="img-fluid" alt="..."> --}}
