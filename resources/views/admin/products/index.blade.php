@extends('layouts.dashboard')
@section('content')

    <x-content-header route="{{ route('products.index') }}" />
    <x-content-subheader>
        <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add a new product
        </x-button-add>
        <x-modal id="create-modal" method="post" action="{{ route('products.store') }}">
            <x-slot name="header">Add a new product</x-slot>
            <x-slot name="body">
                @include('admin._forms.create-products')
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="Products ">
        <x-slot name="header">
            <x-header-dates route="{{ route('products.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('name','name ') </th>
                <th scope="col">@sortablelink('ar_name','ar name ') </th>
                <th scope="col">@sortablelink('fr_name','fr name ') </th>
                <th scope="col">@sortablelink('image','Image') </th>

            </tr>

        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-item type="button" type="button" data-bs-toggle="modal"
                                data-bs-target="#show-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('products.show', $row->id) }}">
                                View product
                            </x-operation-item>
                            <x-operation-button type="button">
                                <a href="{{ route('products.preview', $row->id) }}">Preview product</a>
                                </x-operation-item>
                                <x-operation-button type="button" type="button" data-bs-toggle="modal"
                                    data-bs-target="#edit-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                    data-url="{{ route('products.update', $row->id) }}">
                                    Edit product
                                </x-operation-button>
                                <x-operation-button type="button" data-bs-toggle="modal"
                                    data-bs-target="#delete-modal-{{ $row->id }}" class="delete-user"
                                    data-id="{{ $row->id }}" data-url="{{ route('products.destroy', $row->id) }}">
                                    Delete product
                                </x-operation-button>

                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->ar_name }}</td>
                    <td>{{ $row->fr_name }}</td>
                    <td>
                        <img width="65" src="{{ asset($row->image) }}" class="zoom"
                            alt="{{ $row->ar_title }}">
                    </td>

                </tr>
                <x-modal id="show-modal-{{ $row->id }}" method="get" role="document"
                    action="{{ route('products.show', $row->id) }}">
                    <x-slot name="header">View product</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.view-products')
                    </x-slot>
                </x-modal>



                <x-modal id="delete-modal-{{ $row->id }}" method="delete" role="document"
                    action="{{ route('products.destroy', $row->id) }}">
                    <x-slot name="header">Do you want to delete this product ?</x-slot>
                    <x-slot name="body">
                    </x-slot>
                </x-modal>
                <x-modal  id="edit-modal-{{ $row->id }}" method="put" role="document"
                    formid="Editproduct{{ $row->id }}"  rowid="{{$row->id}}" 
                    action="{{ route('products.update', $row->id) }}">
                    <x-slot name="header">Edit product</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.update-products')

                    </x-slot>
                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection
