@extends('layouts.dashboard')
@section('content')
    <!-- Theme included stylesheets -->
    <link href="/static/favicon.ico" rel="icon" type="image/x-icon" />
    <link
      href="https://cdn.quilljs.com/2.0.0-dev.4/quill.snow.css"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/styles/github.min.css"
    />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/highlight.min.js"></script>
    <script
      charset="UTF-8"
      src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/languages/xml.min.js"
    ></script>
    <script src="https://cdn.quilljs.com/2.0.0-dev.4/quill.min.js"></script>

    <script
      src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"
      crossorigin="anonymous"
    ></script>
    <!--  DEVELOMENT  <script src="../../../dist/quill.htmlEditButton.min.js"></script> -->
    <script src="https://unpkg.com/quill-html-edit-button@2.2.7/dist/quill.htmlEditButton.min.js""></script>

    <script>
      Quill.register("modules/htmlEditButton", htmlEditButton);

      const fullToolbarOptions = [
        [{ header: [1, 2, 3, false] }],
        ["bold", "italic"],
        ["clean"],
        ["image"],
      ];

      console.log("Demo loaded...");

      var quill = new Quill("#editor", {
        theme: "snow",
        modules: {
          toolbar: {
            container: fullToolbarOptions,
          },
          htmlEditButton: {
            syntax: true,
          },
        },
      });
    </script>

<x-content-header route="{{route('news.index')}}"/>
<x-content-subheader>
    <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
    Add a new post
    </x-button-add>
    <x-modal id="create-modal"  method="post" action="{{ route('news.store') }}">
        <x-slot name="header">Add a new post</x-slot>
        <x-slot name="body">
            @include('admin._forms.create-news')
        </x-slot>
    </x-modal>

</x-content-subheader>
<x-table title="Posts ">
    <x-slot name="header">
        <x-header-dates route="{{route('news.index')}}"/>
    </x-slot>
    <x-slot name="tableheader">
        <tr>
            <th scope="col"></th>
            <th scope="col">@sortablelink('id', '#')</th>
            <th scope="col">@sortablelink('name','Title ') </th>
            <th scope="col">@sortablelink('ar_name','ar Title ') </th>
            <th scope="col">@sortablelink('fr_name','fr Title ') </th>
            <th scope="col">Image</th>

        </tr>

    </x-slot>
    <x-slot name="tablebody">
        @foreach ($data as $row)
            <tr>
                <th scope="row">
                    <x-operations>
                        <x-operation-item type="button" type="button" data-bs-toggle="modal" data-bs-target="#show-modal-{{$row->id}}"
                            data-id="{{$row->id}}" data-url="{{ route( 'news.show', $row->id) }}">
                            View post
                        </x-operation-item>
                        <x-operation-button type="button" type="button" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$row->id}}"
                            data-id="{{$row->id}}" data-url="{{ route( 'news.update', $row->id) }}">
                            Edit post
                        </x-operation-button>
                        <x-operation-button type="button" data-bs-toggle="modal" data-bs-target="#delete-modal-{{$row->id}}" class="delete-user"
                            data-id="{{$row->id}}" data-url="{{ route( 'news.destroy', $row->id) }}">
                            Delete post
                        </x-operation-button>

                    </x-operations>


                </th>
                <td>{{ $row->id }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->ar_name }}</td>
                <td>{{ $row->fr_name }}</td>
                <td>
                <img width="65" src="{{asset($row->image) }}" class="zoom" alt="{{ $row->ar_title }}">
            </td>

            </tr>
            <x-modal id="show-modal-{{$row->id}}"  method="post" role="document" action="{{ route('news.show',$row->id )}}" >
                <x-slot name="header">View post</x-slot>
                <x-slot name="body">
                    @include('admin._forms.view-news')
                </x-slot>
            </x-modal>

            <x-modal id="delete-modal-{{$row->id}}" method="delete" role="document"   
                action="{{route('news.destroy',$row->id)}}" >
                    <x-slot name="header">Do you want to delete this post ?</x-slot>
                    <x-slot name="body">
                    </x-slot>
            </x-modal>
            <x-modal id="edit-modal-{{$row->id}}" method="put" formid="news{{$row->id}}" role="document" action="{{ route('news.update',$row->id )}}" >
                <x-slot name="header">Edit new</x-slot>
                <x-slot name="body">
                    @include('admin._forms.edit-news')
                </x-slot>

            </x-modal>
        @endforeach
    </x-slot>
    <x-slot name="tablefooter">
        <div class="d-flex justify-content-center mt-2  ">
            {{ $data->links('vendor.pagination.bootstrap-4') }}
        </div>
    </x-slot>
</x-table>


@endsection