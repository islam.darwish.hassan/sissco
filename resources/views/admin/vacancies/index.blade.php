@extends('layouts.dashboard')
@section('content')

    <x-content-header />
    <x-content-subheader>
        <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add new Job Vacancy
        </x-button-add>
        <x-modal id="create-modal" formid="createVacancy" method="post" action="{{ route('vacancies.store') }}">
            <x-slot name="header">Add new Job Vacancy</x-slot>
            <x-slot name="body">
                @include('admin._forms.create-vacancy')
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="Vacancies ">
        <x-slot name="header">
            <x-header-dates route="{{ route('vacancies.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('title','title ') </th>
                <th scope="col">@sortablelink('ar_title','ar title ') </th>
                <th scope="col">@sortablelink('fr_title','fr title ') </th>
                <th scope="col">Small Description</th>
                <th scope="col">Image</th>

            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#view-modal-{{ $row->id }}">
                                View vacancy
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#edit-modal-{{ $row->id }}">
                                edit vacancy
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#delete-modal-{{ $row->id }}" class="delete-banner"
                                data-id="{{ $row->id }}" data-url="{{ route('vacancies.destroy', $row->id) }}">
                                delete vacancy
                            </x-operation-button>
                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->title }}</td>
                    <td>{{ $row->ar_title }}</td>
                    <td>{{ $row->fr_title }}</td>
                    <td><a href="">
                            Show in website
                        </a>
                    </td>
                    <td><img width="65" src="{{ asset($row->image) }}" class="zoom"
                            alt="{{ $row->ar_title }}">
                    </td>
                </tr>
                <x-modal id="view-modal-{{ $row->id }}" method="get" role="document"
                    action="{{ route('products.show', $row->id) }}">
                    <x-slot name="header">View vacancy</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.view-vacancy')
                    </x-slot>
                </x-modal>

                <x-modal id="edit-modal-{{ $row->id }}" method="put" role="document"
                    formid="Editvacancy{{ $row->id }}"  rowid="{{$row->id}}" 
                    action="{{ route('vacancies.update', $row->id) }}">
                    <x-slot name="header">Edit vacancy</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.edit-vacancy')
                    </x-slot>
                </x-modal>

                <x-modal id="delete-modal-{{ $row->id }}" method="delete" role="document"
                    action="{{ route('vacancies.destroy', $row->id) }}">
                    <x-slot name="header">Do you want to delete this vacancy</x-slot>
                    <x-slot name="body">
                    </x-slot>
                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection



<style>
    .zoom {
        ransition: transform .2s;
        /* Animation */
        width: 65px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(2.5);
        /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

</style>
