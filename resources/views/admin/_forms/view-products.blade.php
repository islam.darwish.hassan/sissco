<img class="img-thumbnail " title="image" src="{{ $row->image }}" />
<x-input label="En name" type="text" value="{{ $row->name }}" disabled />
<x-input label="Ar name" type="text" value="{{ $row->ar_name }}" disabled />
<x-input label="FR name" type="text" value="{{ $row->fr_name }}" disabled />
<x-input label="Category" type="text" value="{{ $row->category->name }}" disabled />

<x-w-textarea label="
Description" style="white-space: normal" type="text" value="{{ $row->description }}" disabled>
    {{ strip_tags($row->description) }}
</x-w-textarea>
<x-w-textarea label="
Fr_Description" style="white-space: normal" type="text" value="{{ $row->fr_description }}" disabled>
    {{ strip_tags($row->fr_description) }}
</x-w-textarea>
<x-w-textarea label="
AR_Description" style="white-space: normal" type="text" value="{{ $row->ar_description }}" disabled>
    {{ strip_tags($row->ar_description) }}
</x-w-textarea>
