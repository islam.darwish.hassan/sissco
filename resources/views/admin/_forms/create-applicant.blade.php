@php
$categories=App\Models\Category::all();
@endphp

<x-select name="category_id">
    <option selected>اختار التخصص</option>
    @foreach($categories as $item )
    <option value="{{$item->id}}">{{$item->name}}</option>
    @endforeach
</x-select>
<x-input label="اسم المتقدم" name="name" type="text" />
<x-input label="السن" name="age" type="number" />
<x-input label="رقم التواصل" name="phone" type="number" />
<x-input label="البريد الالكتدوني للتواصل" name="email" type="text" />
<x-input label="المؤهل" name="email" type="text" />
<x-input label="عدد سنين الخبرة" name="years_of_exp" type="number" />
<x-select name="gender">
    <option selected>اختار النوع</option>
    <option value="m">ذكر</option>
    <option value="m">انثي</option>
</x-select>
<x-select name="military_status">
    <option selected>اختار الحالة من الخدمة العسكرية</option>
    <option value="1">اعفاء طبي</option>
    <option value="2">مؤجل</option>
    <option value="3">اتم الخدمة</option>
    <option value="4">مستحق</option>

</x-select>
<x-select name="social_status">
    <option selected>اختار الحالةالاجتماعية</option>
    <option value="1">اعزب</option>
    <option value="2">متزوج</option>
    <option value="3">مطلق</option>
    <option value="4">ارمل</option>

</x-select>
<x-file-upload  title="أضف السيرة الذاتية" name="cv" />


