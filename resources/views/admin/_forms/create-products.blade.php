<x-input label="En name*" name="name" type="text" />
<x-input label="Ar name" name="ar_name" type="text" />
<x-input label="FR name" name="fr_name" type="text" />
<label for="category_id" class="fw-bold">Choose category *</label>
<x-select name="category_id" id="category">
    <option value=""></option>

    @foreach ($categories as $category)
        <option value="{{ $category->id }}">{{ $category->name }}</option>

    @endforeach

</x-select>
<x-file-upload class="mw-100" title="Product image" name="image" />
<x-rich-descriptions/>