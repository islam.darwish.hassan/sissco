<x-input label="Partner name" name="name" type="text" value="{{ $row->name }}" />
<x-input label="Partner ar name" name="ar_name" type="text" value="{{ $row->ar_name }}" />

<x-input label="Partner fr name" name="fr_name" type="text" value="{{ $row->fr_name }}" />
<label class="form-label form-label-lg">Partner image</label>
<img class="mw-100" title="image" src="{{ $row->image }}" />

<x-file-upload title="New image" name="new_image" />
<x-rich-descriptions id="Editpartner{{ $row->id }}" rowid="{{ $row->id }}" value="{{ $row->description }}"
    arabic="{{ $row->ar_description }}" frensh="{{ $row->fr_description }}" />
