<x-input label="En Title" name="name" type="text" value="{{ $row->name }}" />
<x-input label="Ar Title" name="ar_name" type="text" value="{{ $row->ar_name }}" />
<x-input label="FR Title" name="fr_name" type="text" value="{{ $row->fr_name }}" />
<label class="form-label form-label-lg">Image</label>
<img class="mw-100" title="Post image" name="image" src="{{ $row->image }}" />
<x-file-upload value="{{ $row->image }}" class="mw-100" title="Award image" name="image"
    value="{{ $row->img }}" />
{{-- @php
    $id="news".$row->id
@endphp
        <x-rich-input id="editor{{ $id }}" name="description" 
            title="Description*" />
        <x-rich-input id="editor_fr{{ $id }}" name="fr_description" title="FR Description" />
        <x-rich-input id="editor_ar{{ $id }}" name="ar_description" title="AR Description" />
    </div> --}}


<x-rich-descriptions id="news{{ $row->id }}" value="{{ $row->description }}"
    arabic="{{ $row->ar_description }}" frensh="{{ $row->fr_description }}"
    rowid="{{$row->id}}" 
    >

</x-rich-descriptions>
{{-- <x-w-textarea label="Post description" name="description" type="text" value="{{ $row->description }}">
    {{ $row->description }}</x-w-textarea> --}}
