<x-input label="Partner name" type="text" value="{{ $row->name }}" disabled />
<x-input label="Partner ar name" type="text" value="{{ $row->ar_name }}" disabled />

<x-input label="Partner fr name" type="text" value="{{ $row->fr_name }}" disabled />
<label class="form-label form-label-lg">Partner image</label><br>
<img class="mw-100" title="image" src="{{ $row->image }}" disabled />


<x-w-textarea label="Partner description " type="text" value="{{ $row->description }}" disabled>
    {{ strip_tags($row->description) }}</x-w-textarea>


<x-w-textarea label="Partner Ar description " type="text" value="{{ $row->description }}" disabled>
    {{ strip_tags($row->ar_description) }}</x-w-textarea>
<x-w-textarea label="Partner Fr description " type="text" value="{{ $row->description }}" disabled>
    {{ strip_tags($row->fr_description) }}</x-w-textarea>
