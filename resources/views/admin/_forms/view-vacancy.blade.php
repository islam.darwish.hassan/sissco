<x-input label="Ar title " value="{{ $row->ar_title }}" disabled name="ar_title" type="text" />
<x-input label="title" name="title" value="{{ $row->title }}" disabled type="text" />
<x-input label="Fr title" value="{{ $row->fr_title }}" disabled name="fr_title" type="text" />
<label class="form-label form-label-lg" for="">Image</label><br>
<img src="{{ $row->image }}" alt=""><br>
<label class="form-label form-label-lg" for="">Small image</label><br>
<img src="{{ $row->small_image }}" alt="">
<x-w-textarea disabled label="Vacancy Ar description">
    {{ strip_tags($row->ar_description) }}
</x-w-textarea>
<x-w-textarea disabled label="Vacancy description">
    {{ strip_tags($row->description) }}
</x-w-textarea>
<x-w-textarea disabled label="Vacancy Fr description">
    {{ strip_tags($row->fr_description) }}
</x-w-textarea>
