<x-input label="En Title" type="text" value="{{ $row->name }}" disabled />
<x-input label="Ar name" type="text" value="{{ $row->ar_name }}" disabled />
<x-input label="FR name" type="text" value="{{ $row->fr_name }}" disabled />
<label class="form-label form-label-lg">Image</label><br>
<img class="mw-100" title="image" src="{{ $row->image }}" />
<x-w-textarea label="Post description" type="text" value="{{ $row->description }}" disabled>
    {{ strip_tags($row->description) }}
</x-w-textarea>
<x-w-textarea label="Post Ar description" type="text" value="{{ $row->ar_description }}" disabled>
    {{ strip_tags($row->ar_description) }}
</x-w-textarea>
<x-w-textarea label="Post Fr description" type="text" value="{{ $row->fr_description }}" disabled>
    {{ strip_tags($row->fr_description) }}
</x-w-textarea>
