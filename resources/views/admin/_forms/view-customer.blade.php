<x-input label="Customer name" type="text" value="{{ $row->name }}" disabled />
<x-input label="Customer ar name" type="text" value="{{ $row->ar_name }}" disabled />

<x-input label="Customer fr name" type="text" value="{{ $row->fr_name }}" disabled />
<label class="form-label form-label-lg">Customer image</label> <br>
<img class="mw-100" title="image" src="{{ $row->image }}" disabled />


<x-w-textarea label="Customer description" type="text" value="{{ $row->description }}" disabled>
    {{ strip_tags($row->description) }}
</x-w-textarea>
<x-w-textarea label="Customer Ar description" type="text" value="{{ $row->ar_description }}" disabled>
    {{ strip_tags($row->ar_description) }}
</x-w-textarea>
<x-w-textarea label="Customer Fr description" type="text" value="{{ $row->fr_description }}" disabled>
    {{ strip_tags($row->ar_description) }}
</x-w-textarea>
