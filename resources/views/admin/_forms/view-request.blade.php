<x-input label="Name " value="{{ $row->name }}" disabled />
<x-input label="Email " value="{{ $row->email }}" disabled />
@if ($row->phone)
    <x-input label="Phone " value="{{ $row->phone }}" disabled />
@endif
<x-input label="Subject " value="{{ $row->subject }}" disabled />
<x-w-textarea label="Message " value="{{ $row->message }}" disabled> {{ $row->message }}</x-w-textarea>
