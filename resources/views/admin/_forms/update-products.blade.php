<x-input label="En Title" name="name" type="text" value="{{ $row->name }}" />
<x-input label="Ar Title" name="ar_name" type="text" value="{{ $row->ar_name }}" />
<x-input label="FR Title" name="fr_name" type="text" value="{{ $row->fr_name }}" />
<label for="category_id">Choose category:</label>
<x-select name="category_id" id="category">
    <option value=""></option>

    @foreach ($categories as $category)
        <option value="{{ $category->id }}" selected>{{ $category->name }}</option>

    @endforeach

</x-select>
<label class="form-label form-label-lg">Image</label><br>
<img class="mw-100" title="Product image" src="{{ $row->image }}" />
<x-input class="mw-100" title="Product image" name="new_image" class="form-control" id="image" type="file" />
{{-- <x-rich-descriptions /> --}}
<x-rich-descriptions id="Editproduct{{ $row->id }}" rowid="{{$row->id}}" value="{{ $row->description }}"
    arabic="{{ $row->ar_description }}" frensh="{{ $row->fr_description }}" />
