    <x-input label="English Name *" name="name" type="text" value="{{ $row->name }}" disabled required />
    <x-input label="French Name" name="fr_name" value="{{ $row->fr_name }} " type="text" disabled />
    <x-input label="Arabic Name" name="ar_name" value="{{ $row->ar_name }} " type="text" disabled />
    <label for="" class="form-label form-label-lg">Image</label>
    <br>
    <img src="{{ $row->image }}" alt="">
    <br>
    <label for="" class="form-label form-label-lg">Cover</label>
    <br>
    <img src="{{ $row->cover }}" alt="">
    <x-w-textarea label="
    Description" style="white-space: normal" type="text" value="{{ $row->description }}" disabled>
        {{ strip_tags($row->description) }}
    </x-w-textarea>
    <x-w-textarea label="
    Fr_Description" style="white-space: normal" type="text" value="{{ $row->fr_description }}" disabled>
        {{ strip_tags($row->fr_description) }}
    </x-w-textarea>
    <x-w-textarea label="
    AR_Description" style="white-space: normal" type="text" value="{{ $row->ar_description }}" disabled>
        {{ strip_tags($row->ar_description) }}
    </x-w-textarea>
