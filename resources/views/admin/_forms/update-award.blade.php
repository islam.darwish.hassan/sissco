<x-input label="En name" name="name" type="text" value="{{ $row->name }}" />
<x-input label="Ar name" name="ar_name" type="text" value="{{ $row->ar_name }}" />
<x-input label="FR name" name="fr_name" type="text" value="{{ $row->fr_name }}" />
<label class="form-label form-label-lg">Image</label><br>
<img class="mw-100" title="Award image" src="{{ $row->image }}" /><br>
<x-input class="mw-100" title="Award image" name="new_image" class="form-control" id="image" type="file" />
<x-rich-descriptions id="Editaward{{ $row->id }}" rowid="{{ $row->id }}" value="{{ $row->description }}"
    arabic="{{ $row->ar_description }}" frensh="{{ $row->fr_description }}" />
