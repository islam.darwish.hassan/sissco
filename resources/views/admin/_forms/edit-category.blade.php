    <x-input label="English Name *" name="name" type="text" value="{{ $row->name }}" required />
    <x-input label="French Name" name="fr_name" value="{{ $row->fr_name }} " type="text" />
    <x-input label="Arabic Name" name="ar_name" value="{{ $row->ar_name }} " type="text" />
    <x-file-upload class="mw-100" title="Image" name="image" />
    <br>
    <img src="{{ $row->image }}" alt="">
    <x-file-upload class="mw-100" title="Cover" name="cover" />
    <br>
    <img src="{{ $row->cover }}" alt="">
    <x-rich-descriptions id="Editcat{{ $row->id }}" rowid="{{$row->id}}" value="{{ $row->description }}"
        arabic="{{ $row->ar_description }}" frensh="{{ $row->fr_description }}" />
