@extends('layouts.dashboard')
@section('content')
    <x-content-header />
    <x-content-title title="Analytics"/>
    <div class="p-3 p-xxl-5">
        <div class="container-fluid px-0">
            {{-- charts --}}
            @include('admin._includes.charts')
        </div>
        <footer class="pt-xxl-5 mt-lg-2">
            <div class="container-fluid px-0 border-top border-gray-200 pt-2 pt-lg-3">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <p class="fs-16 text-gray-600 my-2">2021 &copy; Silicon Arena.com</p>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav navbar">
                            {{-- <li><a href="{{ route('about-us') }}">عن الشركة</a></li>
                            <li><a href="{{ route('home') }}">الذهاب إلي الموقع</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

@endsection
