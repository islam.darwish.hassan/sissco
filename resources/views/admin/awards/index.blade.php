@extends('layouts.dashboard')
@section('content')

    <x-content-header route="{{ route('awards.index') }}" />
    <x-content-subheader>
        <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add a new award
        </x-button-add>
        <x-modal formid="dd" id="create-modal" method="post" action="{{ route('awards.store') }}">
            <x-slot name="header">Add a new award</x-slot>
            <x-slot name="body">
                @include('admin._forms.create-award')
                <x-rich-descriptions id="dd" />
            </x-slot>
        </x-modal>
    </x-content-subheader>

    <x-table title="Awards ">
        <x-slot name="header">
            <x-header-dates route="{{ route('awards.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('name','name ') </th>
                <th scope="col">@sortablelink('ar_name','ar name ') </th>
                <th scope="col">@sortablelink('fr_name','fr name ') </th>
                <th scope="col">@sortablelink('image','Image') </th>

            </tr>

        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-item type="button" type="button" data-bs-toggle="modal"
                                data-bs-target="#show-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('awards.show', $row->id) }}">
                                View award
                            </x-operation-item>
                            <x-operation-button type="button" type="button" data-bs-toggle="modal"
                                data-bs-target="#edit-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('awards.update', $row->id) }}">
                                Edit award
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#delete-modal-{{ $row->id }}" class="delete-user"
                                data-id="{{ $row->id }}" data-url="{{ route('awards.destroy', $row->id) }}">
                                Delete award
                            </x-operation-button>

                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->ar_name }}</td>
                    <td>{{ $row->fr_name }}</td>
                    <td>
                        <img width="65" src="{{ asset($row->image) }}" class="zoom"
                            alt="{{ $row->ar_title }}">
                    </td>

                </tr>
                <x-modal id="show-modal-{{ $row->id }}" method="post" role="document"
                    action="{{ route('awards.show', $row->id) }}">
                    <x-slot name="header">View award</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.view-award')
                    </x-slot>
                </x-modal>

                <x-modal id="delete-modal-{{ $row->id }}" method="delete" role="document"
                    action="{{ route('awards.destroy', $row->id) }}">
                    <x-slot name="header">Do you want to delete this award ?</x-slot>
                    <x-slot name="body">
                    </x-slot>
                </x-modal>

                <x-modal formid="Editaward{{ $row->id }}" id="edit-modal-{{ $row->id }}" method="put"
                    role="document" action="{{ route('awards.update', $row->id) }}">
                    <x-slot name="header">Edit award</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.update-award')
                    </x-slot>

                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection
