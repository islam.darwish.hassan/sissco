@extends('layouts.dashboard')
@section('content')

    <x-content-header route="{{ route('categories.index') }}" />
    <x-content-subheader>
        <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add New Category
        </x-button-add>
        <x-modal formid="Scat" id="create-modal" method="post" action="{{ route('categories.store') }}">
            <x-slot name="header">Add new category</x-slot>
            <x-slot name="body">
                @include('admin._forms.create-category')
                <x-rich-descriptions id="Scat" />
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="Categories ">
        <x-slot name="header">
            <x-header-dates route="{{ route('categories.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">Type</th>
                <th scope="col"> @sortablelink('name','en name')</th>
                <th scope="col">@sortablelink('fr_name','fr name') </th>
                <th scope="col">@sortablelink('ar_name','ar name') </th>
                <th scope="col">@sortablelink('childs_count','subcategories count')</th>
                <th scope="col">@sortablelink('image','Image') </th>
                <th scope="col">@sortablelink('cover','Cover') </th>

            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-item href="{{ route('categories.show', $row->id) }}">
                                view subcategories
                            </x-operation-item>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#edit-modal-{{ $row->id }}">
                                edit category
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#create-modal-sub-{{ $row->id }}">
                                add new subcategory
                            </x-operation-button>
                            <x-operation-form method="delete" action="{{ route('categories.destroy', $row->id) }}">
                                delete category and its subcategories
                            </x-operation-form>
                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->parent_id ? 'SubCategory' : 'Category' }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->fr_name ?? '-' }}</td>
                    <td>{{ $row->ar_name ?? '-' }}</td>
                    <td><a href="{{ route('categories.show', $row->id) }}">{{ $row->childs_count }}</a></td>
                    <td> <img width="65" src="{{ asset($row->image) }}" class="zoom" alt="{{ $row->title }}">
                    </td>
                    <td> <img width="65" src="{{ asset($row->cover) }}" class="zoom" alt="{{ $row->title }}">
                    </td>

                </tr>
                <x-modal formid="Subcat{{ $row->id }}" id="create-modal-sub-{{ $row->id }}" method="post"
                    action="{{ route('categories.storesub', $row->id) }}">
                    <x-slot name="header">Add new subcategory to {{ $row->name }} </x-slot>
                    <x-slot name="body">
                        @include('admin._forms.create-category')
                        <x-rich-descriptions id="Subcat{{ $row->id }}" rowid="{{ $row->id }}" />
                    </x-slot>
                </x-modal>
                <x-modal formid="Editcat{{ $row->id }}" id="edit-modal-{{ $row->id }}" method="put"
                    action="{{ route('categories.update', $row->id) }}">
                    <x-slot name="header">Edit {{ $row->name }} </x-slot>
                    <x-slot name="body">
                        @include('admin._forms.edit-category')
                        <input type="hidden" value={{ $row->id }} name="category" />
                    </x-slot>
                </x-modal>

            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection
