@extends('layouts.dashboard')
@section('content')
    <a href="{{ route('categories.index') }}" class="btn ">
        < Back</a>

            <x-content-header route="{{ route('categories.show', $category->id) }}" />
            <x-content-subheader>
                <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal"
                    data-bs-target="#create-modal-sub-{{ $category->id }}">
                    Add New Subcategory
                </x-button-add>
                <x-modal formid="Subcat{{ $category->id }}" id="create-modal-sub-{{ $category->id }}" method="post"
                    action="{{ route('categories.store') }}">
                    <x-slot name="header">Add new subcategory to {{ $category->name }} </x-slot>
                    <x-slot name="body">
                        @include('admin._forms.create-category')
                        <input type="hidden" value={{ $category->id }} name="category" />
                        <x-rich-descriptions id="Subcat{{ $category->id }}" />
                    </x-slot>
                </x-modal>

            </x-content-subheader>
            <x-table title="{{ $category->name }}">
                <x-slot name="header">
                    <x-header-dates route="{{ route('categories.show', $category->id) }}" />
                </x-slot>
                <x-slot name="tableheader">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">@sortablelink('id', '#')</th>
                        <th scope="col"> @sortablelink('name','en name')</th>
                        <th scope="col">@sortablelink('fr_name','fr name') </th>
                        <th scope="col">@sortablelink('ar_name','ar name') </th>
                        <th scope="col">Image</th>
                    </tr>
                </x-slot>
                <x-slot name="tablebody">
                    @foreach ($data as $row)
                        <tr>
                            <th scope="row">
                                <x-operations>
                                    <x-operation-button type="button" data-bs-toggle="modal"
                                        data-bs-target="#view-modal-{{ $row->id }}">
                                        view sub category
                                    </x-operation-button>
                                    <x-operation-button type="button" data-bs-toggle="modal"
                                        data-bs-target="#edit-modal-{{ $row->id }}">
                                        edit sub category
                                    </x-operation-button>
                                    <x-operation-form method="delete"
                                        action="{{ route('categories.destroy', $row->id) }}">
                                        delete subcategory
                                    </x-operation-form>
                                </x-operations>
                            </th>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->fr_name }}</td>
                            <td>{{ $row->ar_name }}</td>
                            <td><img width="65" src="{{ asset($row->image) }}" class="zoom"
                                    alt="{{ $row->ar_title }}"></td>
                        </tr>
                        <x-modal formid="Editcat{{ $row->id }}" id="edit-modal-{{ $row->id }}" method="put"
                            action="{{ route('categories.update', $row->id) }}">
                            <x-slot name="header">Edit {{ $row->name }} </x-slot>
                            <x-slot name="body">
                                @include('admin._forms.edit-category')
                                <input type="hidden" value={{ $row->id }} name="category" />
                            </x-slot>
                        </x-modal>
                        <x-modal formid="Editcat{{ $row->id }}" id="view-modal-{{ $row->id }}" method="put"
                            action="{{ route('categories.update', $row->id) }}">
                            <x-slot name="header">Edit {{ $row->name }} </x-slot>
                            <x-slot name="body">
                                @include('admin._forms.view-category')
                                <input type="hidden" value={{ $row->id }} name="category" />
                            </x-slot>
                        </x-modal>
                    @endforeach
                </x-slot>
                <x-slot name="tablefooter">
                    <div class="d-flex justify-content-center mt-2  ">
                        {{ $data->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </x-slot>
            </x-table>


        @endsection
