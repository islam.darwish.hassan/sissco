@extends('layouts.dashboard')
@section('content')

    <x-content-header route="{{route('users.index')}}"/>
    <x-content-subheader>
        <x-button-add button class=" btn-primary " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
        Add a new User
        </x-button-add>
        <x-modal id="create-modal" method="post" action="{{ route('users.store') }}">
            <x-slot name="header">Add a new user</x-slot>
            <x-slot name="body">
                @include('admin._forms.create-user')
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="Admin ">
        <x-slot name="header">
            <x-header-dates route="{{route('users.index')}}"/>
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('id', 'id')</th>
                <th scope="col">@sortablelink('name','name') </th>
                <th scope="col">@sortablelink('email','email')</th>
                <th scope="col">@sortablelink('phone','phone')</th>


            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-item type="button" type="button" data-bs-toggle="modal" data-bs-target="#show-modal-{{$row->id}}"
                                data-id="{{$row->id}}" data-url="{{ route( 'users.show', $row->id) }}">
                                View user
                            </x-operation-item>
                            <x-operation-button type="button" type="button" data-bs-toggle="modal" data-bs-target="#edit-modal-{{$row->id}}"
                                data-id="{{$row->id}}" data-url="{{ route( 'users.update', $row->id) }}">
                                Edit user
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal" data-bs-target="#delete-modal-{{$row->id}}" class="delete-user"
                                data-id="{{$row->id}}" data-url="{{ route( 'users.destroy', $row->id) }}">
                                Delete user
                            </x-operation-button>

                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->phone }}</td>

                </tr>
                <x-modal id="show-modal-{{$row->id}}" method="post" role="document" action="{{ route('users.show',$row->id )}}" >
                    <x-slot name="header">View user</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.show-user')
                    </x-slot>
                </x-modal>

                <x-modal id="delete-modal-{{$row->id}}" method="delete" role="document"   
                    action="{{route('users.destroy',$row->id)}}" >
                        <x-slot name="header">Do you want to delete this user ?</x-slot>
                        <x-slot name="body">
                        </x-slot>
                </x-modal>
                <x-modal id="edit-modal-{{$row->id}}" method="put" role="document" action="{{ route('users.update',$row->id )}}" >
                    <x-slot name="header">edit a user</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.edit-user')
                    </x-slot>

                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection
