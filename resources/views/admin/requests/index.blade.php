@extends('layouts.dashboard')
@section('content')

    <x-content-header />
    <x-content-subheader>

    </x-content-subheader>
    <x-table title="Requests ">
        <x-slot name="header">
            <x-header-dates route="{{ route('requests.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('name','name ') </th>
                <th scope="col">@sortablelink('email','email ') </th>
                <th scope="col">@sortablelink('phone','phone ') </th>
                <th scope="col">Subject</th>
                <th scope="col">Message</th>

            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#view-modal-{{ $row->id }}">
                                View request
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#delete-modal-{{ $row->id }}" class="delete-banner"
                                data-id="{{ $row->id }}" data-url="{{ route('vacancies.destroy', $row->id) }}">
                                delete request
                            </x-operation-button>
                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->phone }}</td>
                    <td>{{ $row->subject }}</td>
                    <td>{{ Str::limit($row->message, 50, '...') }} <a  data-bs-toggle="modal"
                            data-bs-target="#view-modal-{{ $row->id }}" style="color: blue">Read more</a> </td>
                </tr>
                <x-modal id="view-modal-{{ $row->id }}" method="get" role="document"
                    action="{{ route('products.show', $row->id) }}">
                    <x-slot name="header">View request</x-slot>
                    <x-slot name="body">
                        @include('admin._forms.view-request')
                    </x-slot>
                </x-modal>


                <x-modal id="delete-modal-{{ $row->id }}" method="delete" role="document"
                    action="{{ route('requests.destroy', $row->id) }}">
                    <x-slot name="header">Do you want to delete this request</x-slot>
                    <x-slot name="body">
                    </x-slot>
                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection



<style>
    .zoom {
        ransition: transform .2s;
        /* Animation */
        width: 65px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(2.5);
        /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

</style>
