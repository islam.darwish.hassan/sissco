@extends('layouts.dashboard')
@section('content')

    <x-content-header />
    <x-content-subheader>
        <x-button-add button class=" btn-success " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add customer
        </x-button-add>
        <x-modal formid="addcustomer" id="create-modal" method="post" action="{{ route('customers.store') }}">
            <x-slot name="header"> Add customer
            </x-slot>
            <x-slot name="body">
                @include('admin._forms.create-customer')
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="customers ">
        <x-slot name="header">
            <x-header-dates route="{{ route('customers.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>

                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('name','name') </th>
                <th scope="col">@sortablelink('ar_name','ar_name ') </th>
                <th scope="col">@sortablelink('fr_name','fr_name ') </th>
                <th scope="col">Image</th>

            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#edit-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('customers.update', $row->id) }}">
                                Edit customer
                            </x-operation-button>

                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#show-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('customers.show', $row->id) }}">
                                Show customer
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#delete-modal-{{ $row->id }}" class="delete-customer"
                                data-id="{{ $row->id }}" data-url="{{ route('customers.destroy', $row->id) }}">
                                Delete customer
                            </x-operation-button>
                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->name }}</td>

                    <td>{{ $row->ar_name }}</td>
                    <td>{{ $row->fr_name }}</td>

                    <td><img width="65" src="{{ asset($row->image) }}" class="zoom"
                            alt="{{ $row->ar_title }}">
                </tr>
                <x-modal formid="Editcust{{ $row->id }}" id="edit-modal-{{ $row->id }}" method="put"
                    action="{{ route('customers.update', $row->id) }}">
                    <x-slot name="header">Edit customer</x-slot>
                    <x-slot name="body"> @include('admin._forms.edit-customer')
                    </x-slot>
                </x-modal>


                <x-modal id="show-modal-{{ $row->id }}" method="get" role="document"
                    action="{{ route('customers.update', $row->id) }}">
                    <x-slot name="header">Show customer</x-slot>
                    <x-slot name="body"> @include('admin._forms.view-customer')
                    </x-slot>
                </x-modal>




                <x-modal id="delete-modal-{{ $row->id }}" method="delete" role="document"
                    action="{{ route('customers.destroy', $row->id) }}">
                    <x-slot name="header">Do you want to delete this customer ?</x-slot>
                    <x-slot name="body">
                    </x-slot>
                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection





<style>
    .zoom {
        ransition: transform .2s;
        /* Animation */
        width: 65px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(3.5);
        /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

</style>
