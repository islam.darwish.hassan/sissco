@extends('layouts.dashboard')
@section('content')

    <x-content-header />
    <x-content-subheader>
        <x-button-add button class=" btn-success " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add partner
        </x-button-add>
        <x-modal id="create-modal" formid="addpartner" method="post" action="{{ route('partners.store') }}">
            <x-slot name="header"> Add partner
            </x-slot>
            <x-slot name="body">
                @include('admin._forms.create-partner')
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="Partners ">
        <x-slot name="header">
            <x-header-dates route="{{ route('partners.index') }}" />
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>

                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('name','name') </th>
                <th scope="col">@sortablelink('ar_name','ar_name ') </th>
                <th scope="col">@sortablelink('fr_name','fr_name ') </th>
                <th scope="col">Image</th>

            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#edit-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('partners.update', $row->id) }}">
                                Edit partner
                            </x-operation-button>

                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#show-modal-{{ $row->id }}" data-id="{{ $row->id }}"
                                data-url="{{ route('partners.show', $row->id) }}">
                                View partner
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#delete-modal-{{ $row->id }}" class="delete-partner"
                                data-id="{{ $row->id }}" data-url="{{ route('partners.destroy', $row->id) }}">
                                Delete partner
                            </x-operation-button>
                        </x-operations>


                    </th>
                    <td>{{ $row->id }}</td>
                    <td>{{ $row->name }}</td>

                    <td>{{ $row->ar_name }}</td>
                    <td>{{ $row->fr_name }}</td>

                    <td><img width="65" src="{{ asset($row->image) }}" class="zoom"
                            alt="{{ $row->ar_title }}">
                </tr>
                <x-modal id="show-modal-{{ $row->id }}" method="put" role="document"
                    action="{{ route('partners.show', $row->id) }}">
                    <x-slot name="header">Edit partner</x-slot>
                    <x-slot name="body"> @include('admin._forms.view-partner')
                    </x-slot>
                </x-modal>


                <x-modal id="edit-modal-{{ $row->id }}" method="put" role="document"
                    formid="Editpartner{{ $row->id }}" rowid="{{ $row->id }}"
                    action="{{ route('partners.update', $row->id) }}">
                    <x-slot name="header">Show partner</x-slot>
                    <x-slot name="body"> @include('admin._forms.edit-partner')
                    </x-slot>
                </x-modal>




                <x-modal id="delete-modal-{{ $row->id }}" method="delete" role="document"
                    action="{{ route('partners.destroy', $row->id) }}">
                    <x-slot name="header"> Do you want to delete this partner ?</x-slot>
                    <x-slot name="body">
                    </x-slot>
                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection





<style>
    .zoom {
        ransition: transform .2s;
        /* Animation */
        width: 65px;
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(3.5);
        /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

</style>
