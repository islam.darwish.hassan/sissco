@extends('layouts.dashboard')
@section('content')
<x-arrow-header />

<div class="p-3 p-xxl-5">
  <div class="container-fluid px-0">
    <div class="row">
      <div class="col-12">
        <div class="card rounded-12 shadow-dark-80 border border-gray-50 mb-3 mb-xl-5 overflow-hidden">
          <img src="../assets/img/placeholder16.svg" alt="Placeholder">
          <div class="card-body p-2 pt-2 p-lg-5 position-relative">
            <div class="p-md-3">
          
              <div class="row">
                <div class="col-12 col-sm-6 col-lg-8">
                  <h3 class="mb-1">{{ auth()->user()->name }}<svg class="ml-1" data-name="Group 1" xmlns="http://www.w3.org/2000/svg" width="26" height="25.19" viewBox="0 0 24 23.25">
                    <path d="M23.823,11.991a.466.466,0,0,0,0-.731L21.54,8.7c-.12-.122-.12-.243-.12-.486L21.779,4.8c0-.244-.121-.609-.478-.609L18.06,3.46c-.12,0-.36-.122-.36-.244L16.022.292a.682.682,0,0,0-.839-.244l-3,1.341a.361.361,0,0,1-.48,0L8.7.048a.735.735,0,0,0-.84.244L6.183,3.216c0,.122-.24.244-.36.244L2.58,4.191a.823.823,0,0,0-.48.731l.36,3.412a.74.74,0,0,1-.12.487L.18,11.381a.462.462,0,0,0,0,.732l2.16,2.437c.12.124.12.243.12.486L2.1,18.449c0,.244.12.609.48.609l3.24.735c.12,0,.36.122.36.241l1.68,2.924a.683.683,0,0,0,.84.244l3-1.341a.353.353,0,0,1,.48,0l3,1.341a.786.786,0,0,0,.839-.244L17.7,20.035c.122-.124.24-.243.36-.243l3.24-.734c.24,0,.48-.367.48-.609l-.361-3.413a.726.726,0,0,1,.121-.485Z" fill="#0D6EFD"></path>
                      <path data-name="Path" d="M4.036,10,0,5.8,1.527,4.2,4.036,6.818,10.582,0,12,1.591Z" transform="translate(5.938 6.625)" fill="#fff"></path>
                    </svg>
                  </h3>
                  
                  <p class="text-gray-700 mb-1 lh-base">{{auth()->user()->email}}</p>
                  
                  @if(is_null(auth()->user()->phone))
                
                  
                 
               
                  @else
                    <p class="mb-md-0"><svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-phone-call" width="22" height="22" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"/> <path d="M5 4h4l2 5l-2.5 1.5a11 11 0 0 0 5 5l1.5 -2.5l5 2v4a2 2 0 0 1 -2 2a16 16 0 0 1 -15 -15a2 2 0 0 1 2 -2" /> <path d="M15 7a2 2 0 0 1 2 2" /> <path d="M15 3a6 6 0 0 1 6 6" />
                    </svg><span class="text-gray-700">  {{    auth()->user()->phone }}</span>
                    </p>
                @endif
               
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   
    <div class="row">
      <div class="col-xl-16 col-xxl-16">
        <div class="card rounded-12 shadow-dark-80 border border-gray-50 mb-4">
          <div class="d-flex align-items-center px-3 px-md-4 py-3 border-bottom border-gray-200">
            <h5 class="card-header-title mb-0 my-md-2 ps-md-3 font-weight-semibold">Update your info</h5>
          </div>
          <div class="card-body px-0 p-md-4">
            <div class="px-3">
              <form class="row g-3" method="POST" action="{{ route('profile.update', auth()->user()->id) }}">
                @csrf 
                @method('PUT')
                <div class="col-md-6">
                  <label for="name" class="form-label font-weight-semibold" >Name</label>
                  <input type="text" class="form-control" name="name" value="{{auth()->user()->name }}" >
                </div>

               
                <div class="col-md-6">
                  <label for="inputAddress" class="form-label font-weight-semibold">Phone</label>
                  <input type="text" class="form-control" name="phone"  value="{{auth()->user()->phone }}">
                </div>
   
                <div class="col-12">
                  <button type="submit" class="btn btn-primary" >Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xl-16 col-xxl-16">
            <div class="card rounded-12 shadow-dark-80 border border-gray-50 mb-4">
              <div class="d-flex align-items-center px-3 px-md-4 py-3 border-bottom border-gray-200">
                <h5 class="card-header-title mb-0 my-md-2 ps-md-3 font-weight-semibold">Update Your Password</h5>
              </div>
              <div class="card-body px-0 p-md-4">
                <div class="px-3">
                  <form class="row g-3" method="POST" action="{{ route('profile.update_password', auth()->user()->id) }}">
                    @csrf 
                    @method('PUT')
                   
      
                    <div class="col-md-6">
                      <label  class="form-label font-weight-semibold">Old Password</label>
                      <input type="password" class="form-control" name="old_password" >
                    </div>
      
      
          
      
                    <div class="col-md-6">
                      <label  class="form-label font-weight-semibold">New Password  </label>
                      <input  class="form-control" name="NewPassword" type="password">
                    </div>
                  
       
                    <div class="col-12">
                      <button type="submit" class="btn btn-primary" >Update</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
         
                
              </div>
            </div>
          </div>
        </div>
     
            
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>


@endsection