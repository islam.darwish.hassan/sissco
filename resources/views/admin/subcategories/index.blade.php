@extends('layouts.dashboard')
@section('content')

    <x-content-header />
    <x-content-subheader>
        <x-button-add button class=" btn-success " type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
            Add Subcategory
        </x-button-add>
        <x-modal id="create-modal" method="post" action="{{ route('categories.store') }}">
            <x-slot name="header">Add New Category</x-slot>
            <x-slot name="body">
                @include('admin._forms.create-category')
            </x-slot>
        </x-modal>

    </x-content-subheader>
    <x-table title="Categories ">
        <x-slot name="header">
            <x-header-dates route="{{route('categories.index')}}"/>
        </x-slot>
        <x-slot name="tableheader">
            <tr>
                <th scope="col"></th>
                <th scope="col">@sortablelink('ar_name','ar name') </th>
                <th scope="col"> @sortablelink('name','en name')</th>
                <th scope="col"> @sortablelink('name','fr name')</th>
                <th scope="col">@sortablelink('id', '#')</th>
                <th scope="col">@sortablelink('childs_count','Subcategories count')</th>

            </tr>
        </x-slot>
        <x-slot name="tablebody">
            @foreach ($data as $row)
                <tr>
                    <th scope="row">
                        <x-operations>
                            <x-operation-item href="{{ route('categories.show', $row->id) }}">
                                view all subcategories
                            </x-operation-item>
                            <x-operation-button type="button" data-bs-toggle="modal" data-bs-target="#create-modal">
                                add new category
                            </x-operation-button>
                            <x-operation-button type="button" data-bs-toggle="modal"
                                data-bs-target="#create-modal-sub-{{ $row->id }}">
                                add new subcategory
                            </x-operation-button>
                            <x-operation-form method="delete" action="{{ route('categories.destroy', $row->id) }}">
                                delete category and its subcategories
                            </x-operation-form>
                        </x-operations>


                    </th>
                    <td>{{ $row->ar_name }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->fr_name }}</td>

                    <td>{{ $row->id }}</td>
                    <td>{{ $row->childs_count }}</td>
                </tr>

                <x-modal id="create-modal-sub-{{ $row->id }}" method="post" action="{{ route('categories.store') }}">
                    <x-slot name="header">Add new category to {{ $row->name }} </x-slot>
                    <x-slot name="body">
                        @include('admin._forms.create-category')
                        <input type="hidden" value={{ $row->id }} name="category" />
                    </x-slot>
                </x-modal>
            @endforeach
        </x-slot>
        <x-slot name="tablefooter">
            <div class="d-flex justify-content-center mt-2  ">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </x-slot>
    </x-table>


@endsection
