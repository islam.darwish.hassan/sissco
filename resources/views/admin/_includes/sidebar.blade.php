<nav class=" navbar navbar-vertical navbar-expand-lg navbar-light initial-hide" id="body">
    <a class="navbar-brand mx-auto d-none d-lg-block my-0 my-lg-4" href="#">
        <img src="{{ asset('logo.png') }}" alt="Logo">
        <img src="../assets/svg/brand/logo-white.svg" alt="Logo" class="white-logo2">
        <img src="{{ asset('logo.png') }}" class="muze-icon" alt="logo">
        <img src="../assets/svg/brand/muze-icon-white.svg" class="muze-icon-white" alt="Logo">
    </a>
    <div class="navbar-collapse">
        <ul class="navbar-nav mb-2" id="appAccordion" data-simplebar>
            <x-sidebar-item class="{{ request()->routeIs('dashboard') ? 'active' : '' }}" title="Dashboard"
                route="{{ route('dashboard') }}">
                <x-svg-charts />
            </x-sidebar-item>
            <li class="nav-item nav-subtitle">
                <small>Pages</small>
            </li>
            <x-sidebar-item class="{{ request()->routeIs('categories.index') ? 'active' : '' }}" title="Categories "
                route="{{ route('categories.index') }}">
                <x-svg-page />
            </x-sidebar-item>
            <x-sidebar-item class="{{ request()->routeIs('products.index') ? 'active' : '' }}" title="Products " route="{{ route('products.index') }}">
                <x-svg-cable />
            </x-sidebar-item>
            <x-sidebar-item class="{{ request()->routeIs('awards.index') ? 'active' : '' }}" title="Awards "
                route="{{ route('awards.index') }}">
                <x-svg-apps />
            </x-sidebar-item>
            <x-sidebar-item class="" title="Services " route="{{ route('services.index') }}">
                <x-svg-speed />
            </x-sidebar-item>
            <x-sidebar-item class=" request()->routeIs('news.index') ? 'active' : '' " title="News " route="{{ route('news.index') }}">
                <x-svg-cable />
            </x-sidebar-item>
            <x-sidebar-item class="" title="Recieved Job Requests " route="{{ route('requests.index') }}">
                <x-svg-apps />
            </x-sidebar-item>
            <x-sidebar-item class="{{ request()->routeIs('vacancies.index') ? 'active' : '' }}" title="Job Vacancies "
                route="{{ route('vacancies.index') }}">
                <x-svg-speed />
            </x-sidebar-item>
            <x-sidebar-item class="{{ request()->routeIs('users.index') ? 'active' : '' }}" title="Users "
                route="{{ route('users.index') }}">
                <x-svg-cable />
            </x-sidebar-item>
            <x-sidebar-item class="" title="Partners " route="{{ route('partners.index') }}">
                <x-svg-speed />
            </x-sidebar-item>
            <x-sidebar-item class="" title="Customers " route="{{ route('customers.index') }}">
                <x-svg-speed />
            </x-sidebar-item>

        </ul>

    </div>
</nav>
