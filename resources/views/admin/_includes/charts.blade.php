<div class="row">
    <x-chart-card-t4 id="chart1" title="Categories" :datasets="$chart_1" :number="$chart_1_number" :labels="$months" />
    <x-chart-card-t4 id="chart2" title="Products " :datasets="$chart_2" :number="$chart_2_number" :labels="$months" />
    <x-chart-card-t4 id="chart3" title="Awards" :datasets="$chart_3" :number="$chart_3_number" :labels="$months" />
    <x-chart-card-t4 id="chart4" title="Services" :datasets="$chart_4" :number="$chart_4_number" :labels="$months" />
    <x-chart-card-t4 id="chart5" title="News" :datasets="$chart_5" :number="$chart_5_number" :labels="$months" />
    <x-chart-card-t4 id="chart6" title="Vacancies" :datasets="$chart_6" :number="$chart_6_number" :labels="$months" />



</div>
<div class="row">
    <x-chart-card title="Requests" :labels="$months" :datasets="$chart_7" :secdatasets="$chart_12" :number="$chart_7_number" id="chart7" />
    <x-chart-card title="Partners" :labels="$months" :datasets="$chart_8" :secdatasets="$chart_11" :number="$chart_8_number" id="chart8" />
</div>
<div class="row">
        <x-chart-card-t3 title="Customers" id="chart9" :datasets="$chart_9" :secdatasets="$chart_10" :labels="$months" />
    </div>