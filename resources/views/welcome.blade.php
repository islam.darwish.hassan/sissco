@extends('layouts.website')
@section('content')
<div class="w-100">
    @include('_includes.navbar')
    @include('_includes.topnav')

    @include('_includes.carousel')
    <div class="my-5">
        @include('_includes.businesslines')
    </div>
    <div class="my-5">
        @include('_includes.marketing')
    </div>
    @include('_includes.global_map')
    <div class="my-5">
        @include('_includes.careers')
    </div>
    @include('_includes.latest_news')
</div>
@endsection