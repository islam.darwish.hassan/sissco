@extends('layouts.website')
@section('content')

@include('_includes.navbar')
@include('_includes.topnav')

<img src={{$category->cover}} class="image-cover"/>
    <div >
        @include('_includes.category.description')
    </div>
    <br/>
    
@endsection