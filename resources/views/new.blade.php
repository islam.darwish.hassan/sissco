@extends('layouts.website')
@section('content')

@include('_includes.navbar')
@include('_includes.topnav')

<img src={{$new->image}} class="image-cover"/>

<x-w_subheader>
    <h2 class="p-2 fw-bold"> {{$new->name}}</h2>
</x-w_subheader>

<div class="container-fluid my-2 px-5">
    {!!$new->description!!}
</div>
@endsection