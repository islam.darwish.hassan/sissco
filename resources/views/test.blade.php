
@extends('layouts.dashboard')
@section('content')
    <!-- Theme included stylesheets -->
    <link href="/static/favicon.ico" rel="icon" type="image/x-icon" />
    <link
      href="https://cdn.quilljs.com/2.0.0-dev.4/quill.snow.css"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/styles/github.min.css"
    />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/highlight.min.js"></script>
    <script
      charset="UTF-8"
      src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/languages/xml.min.js"
    ></script>
    <script src="https://cdn.quilljs.com/2.0.0-dev.4/quill.min.js"></script>


  <div>
      <x-modal>
    <x-rich-descriptions id="addcustomer" />

    <script
      src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"
      crossorigin="anonymous"
    ></script>
    <!--  DEVELOMENT  <script src="../../../dist/quill.htmlEditButton.min.js"></script> -->
    <script src="https://unpkg.com/quill-html-edit-button@2.2.7/dist/quill.htmlEditButton.min.js""></script>

    <script>
      Quill.register("modules/htmlEditButton", htmlEditButton);

      const fullToolbarOptions = [
        [{ header: [1, 2, 3, false] }],
        ["bold", "italic"],
        ["clean"],
        ["image"],
      ];

      console.log("Demo loaded...");

      var quill = new Quill("#editor", {
        theme: "snow",
        modules: {
          toolbar: {
            container: fullToolbarOptions,
          },
          htmlEditButton: {
            syntax: true,
          },
        },
      });
    </script>
  </div>
@endsection