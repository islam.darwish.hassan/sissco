<x-w_subheader>
    <h2 class="p-2 fw-bold"> {{$category->name}}</h2>
</x-w_subheader>
<div class="container-fluid my-2 px-5">
    {!! $category->description!!}
    </div>

@if($categories->count() >0)
<div class="my-5">
    @include('_includes.businesslines')
</div>
@endif

@if($category->services->count() >0)
<x-w_subheader>
    <h2 class="p-2 fw-bold"> Services</h2>
</x-w_subheader>
@endif
<div class=" businessContainer" >
    <div class="imageGrid row ">
        @foreach ( $category->services as $row )
            
        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class=" wow fadeInDown imageCard mt-2" data-wow-delay="{{strval($loop->index*100)."ms"}}">
                <img src="{{ $row->image }}"  alt=" ...">
                <div class="{{$loop->index<6?"wow slideInLeft":""}} captionContainer" onclick="window.location='{{ route('service.show',$row->id ) }}'">
                    <p >{{$row->name}}</p>
                    <img src="{{ asset('images/Icon feather-arrow-right@2x.png') }}" style="height: 20px">
                </div>


                <div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@if($category->products->count() >0)
<x-w_subheader>
    <h2 class="p-2 fw-bold"> Products</h2>
</x-w_subheader>
@endif
<div class=" businessContainer" >
    <div class="imageGrid row ">
        @foreach ( $category->products as $row )
            
        <div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-xs-6">
            <div class=" wow fadeInDown imageCard mt-2" data-wow-delay="{{strval($loop->index*100)."ms"}}">
                <img src="{{ $row->image }}"  alt=" ...">
                <div class="{{$loop->index<6?"wow slideInLeft":""}} captionContainer" onclick="window.location='{{ route('wproduct.show',$row->id ) }}'">
                    <p >{{$row->name}}</p>
                    <img src="{{ asset('images/Icon feather-arrow-right@2x.png') }}" style="height: 20px">
                </div>


                <div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

