<div class="mt-5"></div>
<x-w_subheader>
    <h1 class="p-2 fw-bold "> About Us</h1>
</x-w_subheader>
<x-w_subheader>
    <h2 class="p-2 fw-bold "> History</h2>
</x-w_subheader>
<div class="container-fluid">
    <p class="px-5">
        Siescom Group since establishment in 2011 in cairo Egypt working in the telecom and civil insdusty ,
        in 2012 Siescom decieded to start its journey. In the african market till we reached operations in 17 different
        country,

        During these years, the group decided to expand its investment circle to include the aluminum and iron industry,
        trade, construction and infrastructure.

    </p>
    <p class="px-5">
        We pride ourselves in providing a complete solution for all our clients Needs as we are giving the same level of
        attention with no matter what the project size is.
        All our projects are handled by highly qualified Engineers and Technicians who Are well trained and have enough
        Experience in the field. Our technical Staff ranges from entry to senior level,Depending on the needs of our
        clients.
        We have an extremely talented and dedicated team of professionals that are well trained, in meeting and beating
        our client's expectations.
        We like to think of our clients as our partners, which helps stimulate an atmosphere of mutual growth, This
        distinct philosophy further urges our professionals to contribute extensively and take pride in the client's
        success through the services provided to them.
    </p>

</div>
<x-w_subheader>
    <h2 class="p-2 fw-bold "> Vision</h2>
</x-w_subheader>
<div class="container-fluid">
    <p class="px-5">We believe that the customer plays a significant role in our existence, that is why our
        service is personal, While our partner is centralized and treated with respect and integrity at all times.
    </p>
    <p class="px-5">
        Our associates are professionals that can advise and deliver projects according to deadlines, and always
        complying with their promises, without any excuse. For that reason, we are committed to constantly develop
        ourselves not only as a company, but also personally and collectively to always offer the client the best
        service that they deserve.
    </p>
</div>
<x-w_subheader>
    <h2 class="p-2 fw-bold"> Mission</h2>
</x-w_subheader>
<div class="container-fluid">
    <p class="px-5">
        To continue to develop ourselves and expand the circle of our services to ensure integrated solutions and more
        advanced products on the latest international systems with the best efficiency and competitive prices that
        ensure a strong presence in the market, which contributes to the growth and prosperity of the communities where
        we are working .
    </p>
</div>
<x-w_subheader>
    <h2 class="px-2 fw-bold"> Partners</h2>
</x-w_subheader>
<div class="container-fluid px-5">
    <div class="row ">
        @foreach ($partners as $row)
            <x-loop-animate :loop="$loop" class="col-lg-2 col-6">
                <img class="partners-image " src={{ $row->image }}>
            </x-loop-animate>
        @endforeach
    </div>
</div>
<x-w_subheader>
    <h2 class="p-2 fw-bold "> Customers</h2>
</x-w_subheader>
<div class="container-fluid">
    <div class=" row px-5">
        @foreach ($customers as $row)
            <img class="partners-image col-lg-4" src={{ $row->image }}>
        @endforeach
    </div>
</div>
