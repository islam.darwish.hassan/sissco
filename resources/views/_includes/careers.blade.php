<div class="w-100 careers">

    <x-w_subheader>
        <h2 class="p-2 fw-bold"> CAREERS</h2>
    </x-w_subheader>

    <div class="content-overlay">
        <div class="content-overlay2">
            <div class="content-container">
            <h2 a href="{{route('contact.us')}}">Join us</h2>
            <p>We employ around 15,000 people, with operations in numerous countries around the world.
                Come and join the team.
            </p>
            <x-button class="btn-light h-100 btn-transtion " >
               <a href="{{route('contact.us')}}"> Join With Us</a>
            </x-button>
            </div>
        </div>

    </div>

</div>
