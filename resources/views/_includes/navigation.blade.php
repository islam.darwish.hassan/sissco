<nav class="navbar navbar-expand-lg navbar-light border-top mb-0 p-3 shadow-sm">
    <div class="container d-flex justify-content-between">
        <button class="invisible navbar-toggler border-0  float-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon  "></span>
          </button>
                <a href="{{route('home')}}" class="navbar-brand  d-lg-none " href="#"><img height=50 src={{asset('logo.png')}}></a>

      <button class="navbar-toggler border-0  float-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon  "></span>
      </button>

      <div class="collapse navbar-collapse   " id="navbarSupportedContent">
        <a href="{{route('home')}}" class="navbar-brand filler  d-none d-lg-block " href="#"><img height=50 src={{asset('logo.png')}}></a>
        <ul class="navbar-nav  mb-2 mb-lg-0 d-flex justify-content-between ">
            <x-nav-item
            name="Home"
            link="home"
            />
            <x-nav-item
            name="About us"
            link="about-us"
            active
            />
            <x-nav-item
            name="Our Services"
            link="our-services"
            />
            <x-nav-item
            name="Upload Your CV"
            link="upload-your-cv"
            />
            <x-nav-item
            name="Vacancies"
            link="vacancies"
            />
            <x-nav-item
            name="Contact Us"
            link="contact-us"
            />
        </ul>
        <div class="filler"></div>
        <div class="filler"></div>

      </div>

    </div>

  </nav>
