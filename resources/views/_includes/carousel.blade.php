<div id="carouselIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner">
        @foreach ($banners as $banner)
            @if ($loop->first)
                <div class="carousel-item active">
                    <div class="carousel-content wow fadeInDown">
                        <div class="content">
    
                        <h1>{{$banner->title ??"Siescom"}}</h1>
                        <span>{{$banner->sub_title??"INTEGRATED ENGINEERING SOLUTIONS"}}</span>
                        <p class="mt-2">{{$banner->text??"Our growth has been based on sound finances and a commitment to hiring talented candidates. In addition to empowering businesses and communities, we have contributed significantly to the Egyptian, African and Middle East economies."}} </p>
                        </div>
    
                    </div>
                    <img class="d-block banner-item" src="{{ $banner->image }}" alt="{{ $banner->ar_title }}">
                </div>
            @else

                <div class="carousel-item " data-bs-interval="2000">
                    <div class="carousel-content wow fadeInDown">
                        <div class="content">
    
                        <h1>{{$banner->title ??"Siescom"}}</h1>
                        <span>{{$banner->sub_title??"INTEGRATED ENGINEERING SOLUTIONS"}}</span>
                        <p class="mt-2">{{$banner->text??"Our growth has been based on sound finances and a commitment to hiring talented candidates. In addition to empowering businesses and communities, we have contributed significantly to the Egyptian, African and Middle East economies."}} </p>
                        </div>
    
                    </div>
                   <a href={{$banner->url??false}}> <img class=" banner-item" src="{{ $banner->image }}" alt="{{ $banner->ar_title }}">
                   </a>
                </div>
            @endif
        @endforeach
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
      
    </div>
</div>
