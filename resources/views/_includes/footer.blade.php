<footer class="container-fluid p-5 bg-light-white">
    <div class="row">
        <div class="col-lg-2 col-md-3 col-6">
            <h4>SITE SECTIONS</h4>
            <ul class="footer-links ">
                <li ><a href="{{route('home')}}">HOME </a></li>
                <li ><a href={{route('about-us')}}>ABOUT US</a></li>
                <li ><a href={{route('our-services')}}>OUR SERVICES</a></li>
                <li ><a href={{route('upload-your-cv')}}>UPLOAD YOUR CV</a></li>
                <li ><a href={{route('vacancies')}}>VACANCIES</a></li>
                <li ><a href={{route('contact-us')}}>CONTACT US</a></li>

            </ul>
        </div>

            <div class="col-lg-5 col-md-4 col-6 ">
                <h4>CONTACT US</h4>
                <ul class="footer-links">
                    <li ><span>13 Al Mashtal Street - First Corniche Maadi in front of the Lawyers Club </span></li>
                    <li ><span>MOBILE:   01112170508 - 01227461117 - 01113333175</span></li>
                    <li ><span>PHONE:   0225286090 - 0225286091 - 0225286092</span></li>
                    <li ><span>Email:  afifyco@hotmail.com</span></li>

                </ul>
            </div>

            <div class="col-lg-5 col-md-5 col-12" id="googleMap"  style="height: 350px"></div>

        </div>

    </div>


</footer>

<div class="bg-primary text-white text-center py-2 small fw-lighter"> Developed By <a href="https://silicon-arena.com" class="text-white" target="_blank"> Silicon-Arena </a> For software solutions  </div>


<script>
    function myMap() {
    const uluru = { lat: 29.99251224065914, lng: 31.23017802638847 };
      // The map, centered at Uluru
      const map = new google.maps.Map(document.getElementById("googleMap"), {
        zoom: 20,
        center: uluru,
        mapTypeControl: false
      });
      const svgMarker = {
    path:
      "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
    fillColor: "#006DFF",
    fillOpacity: 1,
    strokeWeight: 0,
    rotation: 0,
    scale: 2,
    anchor: new google.maps.Point(15, 30),
  };
      // The marker, positioned at Uluru
      const marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: svgMarker,

      });
    }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9lwRaQPyQ4ZDOYN7cfhsl8HaCvyjpaL8&callback=myMap"
    async
    ></script>
