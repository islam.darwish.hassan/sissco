    <div class="tabsContainer" id="hide">
        <div class="row">
            <div class="col-lg-4 p-0">
                <div class="tab " onmouseover="showAboutMenu()" onmouseout="disableAboutMenu()"> <a
                        class="mr-3" href="#">About Us</a>
                </div>
            </div>
            <div class="col-lg-4 p-0">
                <div class="tab " onmouseover="showMenu()" onmouseout="disableMenu()"> <a class="mr-3"
                        href="#">BUSINESS LINES</a>
                </div>
            </div>
            {{-- <div class="col-lg-3 p-0">
                <div class="tab p-0"> <a class="mr-3" href="{{route('awardsa.index')}}">AWARDS</a>
                </div>
            </div> --}}
            <div class="col-lg-4 p-0">
                <div class="tab" onmouseover="showGlobalMenu()" onmouseout="disableGlobalMenu()"> <a
                        class="mr-3" href="{{route('home')}}">GLOBAL PRESENCE</a>
                </div>
            </div>
        </div>
    </div>
    @php
        $categories = App\Models\Category::limit(5)->get();
    @endphp
    <div class="menuContainer wow fadeInDown" id="menuContainer" onmouseover="showMenu()" onmouseout="disableMenu()">
        <div class="row p-5">
            @foreach ($categories as $category)
                <ul class="col ">
                    <li class="head"><a href="{{route('category.show',$category->id)}}" class="mb-2  ">{{ $category->name }}</a></li>
                    @foreach ($category->childs as $child)
                        <li class="sub"><a href="{{route('category.show',$child->id)}}">{{ $child->name }}</a></li>
                    @endforeach
                </ul>
            @endforeach

        </div>
    </div>
    <div class="menuContainer wow fadeInDown" id="globalPresenceMenu" onmouseover="showGlobalMenu()" onmouseout="disableGlobalMenu()">
        <div class="row p-5">
            <ul class="col">
                <li class="head"><a href="{{route('home')}}" >SCHEMAS Egypt</a></li>
                <li class="head"><a href="{{route('home')}}" >SCHEMAS Zambia</a></li>
                <li class="head"><a href="{{route('home')}}" >SIESCOM Tanzania Limited</a></li>
                <li class="head"><a href="{{route('home')}}" >SCHEMAS Nigeria</a></li>
                <li class="head"><a href="{{route('home')}}" >SCHEMAS Gabon</a></li>
                <li class="head"><a href="{{route('home')}}" >SIESCOM RDC</a></li>
                <li class="head"><a href="{{route('home')}}" >SIESCOM CONGO</a></li>
                <li class="head"><a href="{{route('home')}}" >SIESCOM Kenya</a></li>

            </ul>
        </div>
    </div>
    <div class="menuContainer wow fadeInDown" id="AboutMenu" onmouseover="showAboutMenu()" onmouseout="disableAboutMenu()">
        <div class="row p-5">
            <ul>
                <li class="head"><a href="{{route('about.us')}}" >About Us</a></li>
                <li class="sub"><a href="{{route('about.us')}}" >Our Vision</a></li>
                <li class="sub"><a href="{{route('about.us')}}" >Our Mission</a></li>
                <li class="sub"><a href="{{route('about.us')}}" >Our Partners</a></li>
                <li class="sub"><a href="{{route('about.us')}}" >Our Customers</a></li>
            </ul>
        </div>
    </div>

    <script>
        function showMenu() {
            document.getElementById('menuContainer').classList.add('menuContainerActive');
        }

        function disableMenu() {
            document.getElementById('menuContainer').classList.remove('menuContainerActive');
        }

        function showGlobalMenu() {
            document.getElementById('globalPresenceMenu').style.display = 'block';
        }

        function showAboutMenu() {
            document.getElementById('AboutMenu').style.display = 'block';
        }

        function disableGlobalMenu() {
            document.getElementById('globalPresenceMenu').style.display = 'none';
        }

        function disableAboutMenu() {
            document.getElementById('AboutMenu').style.display = 'none';
        }
    </script>
