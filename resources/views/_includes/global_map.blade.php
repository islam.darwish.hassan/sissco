<div>
    <x-w_subheader>
        <h2 class="p-2 fw-bold"> GLOBAL PRESENCE </h2>
    </x-w_subheader>
    <div id="googleMap" style=" height:40rem"></div>

    <script>
        function myMap() {
            // Create a new StyledMapType object, passing it an array of styles,
            // and the name to be displayed on the map type control.
            const styledMapType = new google.maps.StyledMapType(
                [{
                        elementType: "geometry",
                        stylers: [{
                            color: "#333333"
                        }]
                    },
                    {
                        elementType: "labels.text.stroke",
                        stylers: [{
                            color: "#444444"
                        }]
                    },
                    {
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#ffffff"
                        }]
                    },
                    {
                        featureType: "administrative.locality",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#d59563"
                        }],
                    },
                    {
                        featureType: "poi",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#d59563"
                        }],
                    },
                    {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [{
                            color: "#263c3f"
                        }],
                    },
                    {
                        featureType: "poi.park",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#6b9a76"
                        }],
                    },
                    {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [{
                            color: "#38414e"
                        }],
                    },
                    {
                        featureType: "road",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#212a37"
                        }],
                    },
                    {
                        featureType: "road",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#9ca5b3"
                        }],
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [{
                            color: "#746855"
                        }],
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#1f2835"
                        }],
                    },
                    {
                        featureType: "road.highway",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#f3d19c"
                        }],
                    },
                    {
                        featureType: "transit",
                        elementType: "geometry",
                        stylers: [{
                            color: "#2f3948"
                        }],
                    },
                    {
                        featureType: "transit.station",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#d59563"
                        }],
                    },
                    {
                        featureType: "water",
                        elementType: "geometry",
                        stylers: [{
                            color: "#ffffff"
                        }],
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#515c6d"
                        }],
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.stroke",
                        stylers: [{
                            color: "#17263c"
                        }],
                    }

                ], {
                    name: "Styled Map"
                }
            );
            const uluru = [{
                    lat: 30.802498,
                    lng: 26.820553,
                    title: "EGYPT"
                },
                {
                    lat: -13.133897,
                    lng: 27.849332,
                    label: "ZAMBIA"

                },{
                    lat: -6.369028,
                    lng: 34.888822,
                    label: "TANZANIA"
                },{
                    lat:9.081999,
                    lng:8.675277,
                    label: "NIGERIA"
                },
                {
                    lat:-0.803689,
                    lng:11.609444,
                    label: "GABON"
                },
                {
                    lat:-4.038333,
                    lng:21.758664,
                    label: "CONGO"
                },
                {
                    lat:-0.023559,
                    lng:37.906193,
                    label: "KENYA"
                }

            ];


            // The map, centered at Uluru
            const map = new google.maps.Map(document.getElementById("googleMap"), {
                zoom: 3.2,
                center: {
                    lat:15.454166,
                    lng:18.732207	
                },
                mapTypeControl: false,
                disableDefaultUI: true,
                draggable: true,
                zoomControl: true,
                scrollwheel: false,
                disableDoubleClickZoom: true

            });
            //Associate the styled map with the MapTypeId and set it to display.
            map.mapTypes.set("styled_map", styledMapType);
            map.setMapTypeId("styled_map");


            const svgMarker = {
                path: "M10.453 14.016l6.563-6.609-1.406-1.406-5.156 5.203-2.063-2.109-1.406 1.406zM12 2.016q2.906 0 4.945 2.039t2.039 4.945q0 1.453-0.727 3.328t-1.758 3.516-2.039 3.070-1.711 2.273l-0.75 0.797q-0.281-0.328-0.75-0.867t-1.688-2.156-2.133-3.141-1.664-3.445-0.75-3.375q0-2.906 2.039-4.945t4.945-2.039z",
                fillColor: "#695521",
                fillOpacity: 1,
                strokeWeight: 0,
                rotation: 0,
                scale: 2,

                anchor: new google.maps.Point(15, 30),
            };
            // The marker, positioned at Uluru
            uluru.forEach((value) => {
                const marker = new google.maps.Marker({
                    position: value,
                    map: map,
                    // icon: svgMarker,

                });
            })

        }
    </script>
    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9lwRaQPyQ4ZDOYN7cfhsl8HaCvyjpaL8&callback=myMap" async>
    </script>

</div>
