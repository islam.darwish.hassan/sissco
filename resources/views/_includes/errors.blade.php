<div class="mx-2">
@if (session('message'))
    <div class="alert alert-success alert-dismissible fade show mt-2 " role="alert">
        <b>{{ session('message') }}</b>
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger alert-dismissible fade show pe-5" role="alert">
        <b>{{ session('error') }}</b>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show mt-2 px-2">
        {{-- <p><b>Please fix these errors.</b></p> --}}
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</div>
