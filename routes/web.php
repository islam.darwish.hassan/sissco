<?php

use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\AwardsController;

// use App\Http\Controllers\AwardsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\NewsController as ControllersNewsController;
use App\Http\Controllers\NProductController;
use App\Http\Controllers\ServiceController;
use App\Models\Banner;
use App\Models\Customer;
use App\Models\News;
use App\Models\Partner;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//website routes
Route::get('/test',function(){
    return view('test');
});
Route::get('/', function () {
    $banners = Banner::limit(10)->get();
    $categories = App\Models\Category::whereNull('parent_id')->get();
    $news =News::orderBy('created_at')->limit(4)->get();
    return view('welcome', compact('banners', 'categories','news'));
})->name('home');
Route::get('/about-us', function () {
    $partners = Partner::all();
    $customers = Customer::all();
    return view('about-us', compact('partners', 'customers'));
})->name('about.us');
Route::get('/business-lines/{category}', [CategoryController::class, 'show'])->name('category.show');
Route::get('/media', [MediaController::class, 'index'])->name('media.show');
Route::get('/product/{product}', [NProductController::class, 'show'])->name('wproduct.show');
Route::get('/news/{new}', [ControllersNewsController::class, 'show'])->name('newsa.show');
Route::get('/news', [ControllersNewsController::class, 'index'])->name('newsa.index');

Route::get('/services/{service}', [ServiceController::class, 'show'])->name('service.show');
Route::get('/awards', [AwardsController::class, 'index'])->name('awardsa.index');
Route::get('/contact-us', function () {
    return view('contact-us');
})->name('contact.us');
Route::post('/contact-us/store', function () {
    return redirect()->route('home');
})->name('contact.us.submit');

Route::get('/privacy', function () {
    return view('privacy');
})->name('privacy');


//admin routes
Route::group(
    ['prefix' => 'admin', 'namespace' => 'App\Http\Controllers\Admin'],
    function () {
        Route::group(['namespace' => 'Auth'], function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::post('login', 'LoginController@login')->name('login_submit');
            Route::post('logout', 'LoginController@logout')->name('logout');
        });

        Route::group(['middleware' => ["auth"]], function () {
            Route::get('dashboard', 'DashboardController@index')->name('dashboard');
            //categories
            Route::resource('categories', 'CategoriesController')->except('create');
            //awards
            Route::resource('awards', 'AwardsController')->except('create');
            Route::post('categories/subadd/{parent}', 'CategoriesController@storesub')->name('categories.storesub');

            //vacancies
            Route::resource('vacancies', 'VacanciesController');
            Route::resource('requests', 'RequestsController');

            //users
            Route::resource('users', 'UsersController')->except('create');

            Route::resource('profile', 'ProfileController');

            Route::resource('news', 'NewsController');
            Route::resource('products', 'ProductsController');

            Route::get('products/preview_product/{id}', 'ProductsController@preview')->name('products.preview');

            Route::resource('services', 'ServicesController');
            Route::resource('partners', 'PartnersController');
            Route::resource('customers', 'CustomersController');


            Route::get('services/preview_services/{id}', ('ServicesController@preview'))->name('service.preview');



            Route::put('profile/{profile}/update_password', 'ProfileController@update_password')->name('profile.update_password');
        });
    }

);
